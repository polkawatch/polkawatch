# Integrating with Polkawatch

Polkawatch is an Open-Source Community Project supported by Polkadot and Kusama treasuries.

Polkawatch data is for the community to utilise and create awareness about effective decentralization achieved by using
Substrate technologies. 

Polkawatch provides tools to facilitate integrating Polkawatch data into other ecosystem products.

## Who is this information intended for.

This documentation is intended for projects in the following areas:
- Wallets
- Dashboards
- DApps

## Integration Architecture

Polkawatch updates its analytics daily and statistics are published on Distributed Data Packages available via IPFS. An 
openapi v3 specification is used as interface to access the data.

When integrating with Polkawatch your product is only dependent on IPFS infrastructure, which makes the infrastructure 
dependency minimal.

## Integration options

You may integrate at different levels ranging from simply offering a link to polkawatch to embedding analytics visualizations
into your product.

### Linking to Polkawatch DAPP

You can generically link to ```https://polkawatch.app``` or to ```https://[chain].polkawatch.app```
Currently the supported chains are: ```polkadot,kusama```. Parachain support will be coming in 2023.

### Linking to Specific Polkawatch Objects

It is possible to link straight to a Polkawatch DAPP object for which there is analytics. This is thanks to 
the support of Redirects in [Kubo 0.16](https://docs.ipfs.tech/how-to/websites-on-ipfs/redirects-and-custom-404s/#evaluation)
and later which allows initiating a DAPP hosted on IPFS with a particular URL by means of redirecting(rewrite) to the Javascript entrypoint.

The target Polkawatch DAPP URLs:
```
/validation/operator/:operatorId/ 
/validation/node/:validatorId/
/pools/operator/:operatorId/ 
/pools/instance/:poolId/
/nomination/:nominatorId/
```

Note that you need to use the base URL of the required chain.
Note that All IDs are AccountIDs. 

Also note that Identity is used to group objects. For example: a Node Operator such as ```ParaNodes.io``` runs several Validators. In that case
the accountId is the Root Identity of their validator nodes. Same principle is used for Nomination Pools.


### Accessing and Charting Polkawatch Analytics via API.

Polkawatch DDP Client API is published as [NodeJS package](https://www.npmjs.com/package/@polkawatch/ddp-client). 

The documentation includes UI Components example that chart the Polkawatch data.

## Getting integration support.

Do not hesitate to [contact us](https://get.polkawatch.app/asset/3:polkawatch-contact-email) to discuss your integration.