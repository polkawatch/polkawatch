// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { Configuration } from '../src/ddp.module';
import { configure } from '../src/ddp.config';
import jestOpenAPI from 'jest-openapi';
import * as fs from 'fs';
import { DdpIpfs } from '../src/ddp.ipfs.controller';
import { DdpLqsService } from '../src/ddp.lqs.service';
import { DdpTransformationService } from '../src/ddp.transformations.service';
import { DdpChainService } from '../src/ddp.chain.service';

describe('AppController (e2e)', () => {
    let app: INestApplication;

    beforeEach(async () => {

        // Creates the module with a fixture as chain metadata, as we won't have LQS to query
        // for chain metadata during testing
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [ Configuration ],
            controllers: [DdpIpfs],
            providers: [DdpLqsService, DdpTransformationService, DdpChainService, {
                provide: 'CHAIN_METADATA',
                useValue: {
                    Name: 'TestToken',
                    Token: 'TST',
                    ErasPerMonth: 30,
                } }],
        }).compile();

        app = moduleFixture.createNestApplication();

        // Configure the app as in production and setup OpenAPI testing
        jestOpenAPI(configure(app, false));

        await app.init();
    });

    // For convenience we generate the openapi specification document
    // only after having verified that some e2e tests are successful.

    it('Will create openapi specification', async () => {
        const doc = configure(app, true);
        const outPath = 'ddp-api-spec.json';
        fs.writeFile(outPath, JSON.stringify(doc), (error) => {
            if (error) throw error;
        });
    });
});
