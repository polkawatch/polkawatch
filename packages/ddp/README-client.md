# About this API Client

This API allows accessing [Polkawatch](https://polkawatch.app) Analytics and using them in your project. 

Polkawatch DDP implements its API using the openapi v3 standard. The specification is bundled as ```ddp-api-spec.json```.

This client has been generated using [openapi generator](https://openapi-generator.tech/) with 
the [typescript axios](https://openapi-generator.tech/docs/generators/typescript-axios) generator.

Note that you can also use ```ddp-api-spec.json``` and use the same tool to produce another API client in any of the 
many other available languages including rust, c++, java, go, python, etc.

# About Typescript Axios generated client  

This generator creates TypeScript/JavaScript client that utilizes [axios](https://github.com/axios/axios). The generated Node module can be used in the following environments:

Environment
* Node.js
* Webpack
* Browserify

Language level
* ES5 - you must have a Promises/A+ library installed
* ES6

Module system
* CommonJS
* ES6 module system

It can be used in both TypeScript and JavaScript. In TypeScript, the definition should be automatically resolved via `package.json`. ([Reference](http://www.typescriptlang.org/docs/handbook/typings-for-npm-packages.html))

# Client API Documentation

The documentation is packaged with the specification, as such the generated code is self-documented and readable. 
IDEs like IntelliJ will do a great job with autocompletion as you code with the API. 

Typedoc generated documentation can be [browsed here](http://ddp-client-doc.polkawatch.app).

# Basic Example

The following example shows how to configure the API and make a request to retrieve the decentralizaiton
data of one Validator node. 

```javascript
import {
  Configuration,
  PolkawatchApi,
} from '@polkawatch/ddp-client';

const apiVersion = "v2";
const chain = "polkadot";
const basePath = `https://${chain}-${apiVersion}-api.polkawatch.app`;

const api = new PolkawatchApi(new Configuration({basePath}));

async function main(){

  // Retrieves decentralization about a Validator

  const response = await api.ddpIpfsValidatorDetail({
    lastDays: 60,
    validator: '14hM4oLJCK6wtS7gNfwTDhthRjy5QJ1t3NAcoPjEepo9AH67',
    validationType: 'public',
  });

  console.log(response.data);

}

main();
```

Save this example as `test.mjs` and run with `node test.mjs`

# Example React DAPP

An example React DAPP using this API to display UI components/charts can be found in [this Repository](https://gitlab.com/polkawatch/ddp-client-dapp-example).