// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Inject, Injectable, Logger } from '@nestjs/common';
import { DdpLqsService } from './ddp.lqs.service';
import { ChainMetadata } from '@lqs/client';

/**
 * The Chain service contains Chain metadata and also allows for performing chain dependent calculations, such
 * as transforming days to eras.
 */

@Injectable()
export class DdpChainService {

    private readonly erasPerDay;

    constructor(@Inject('CHAIN_METADATA') private chain_meta: ChainMetadata) {
        // do nothing
        this.erasPerDay = chain_meta.ErasPerMonth / 30;
    }

    daysToEras(d) {
        return d * this.erasPerDay;
    }

    metaData() {
        return this.chain_meta;
    }

    /**
     * Blockchains that support staking have too many entities to map in IPFS,
     * In that case we cut the inventory down to the public entities only
     */
    inventoryType() {
        if(this.chain_meta.HasStaking) return 'public';
        else return 'all';
    }

}

/**
 * This factory is required because the actual metadata is async.
 * Nest will help so that this piece of information can be use in constructor above.
 */
export const ChainMetadataService = {
    provide: 'CHAIN_METADATA',
    useFactory: async (lqs: DdpLqsService) => {
        const logger = new Logger('CHAIN_METADATA');
        const meta = await lqs.getAPI().about.chainMeta();
        logger.log(`${meta.data.Name} Chain Metadata Retrieved`);
        meta.data.ErasPerMonth;

        return meta.data;
    },
    inject: [DdpLqsService],
};