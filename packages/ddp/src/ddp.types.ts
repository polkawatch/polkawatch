// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { ApiProperty } from '@nestjs/swagger';

export * from '@lqs/types';
export {
    AboutDataQuery,
    EvolutionQuery,
} from '@lqs/client';

import {
    RewardsByRegion,
    RewardsByCountry,
    RewardsByNetworkProvider,
    RewardsByValidationGroup,
    RewardsByValidationNode,
    RewardsByPoolGroup,
    RewardsByPool,
} from '@lqs/types';

/**
 * The distribution chart type describes the data as requested by DAPP charting library
 * Any distribution query result can be transformed into this data type using the transformation service.
 */
export class DistributionChart {
    @ApiProperty({
        description: 'Distribution Chart Labels',
        isArray: true,
        type: String,
    })
        labels: string[];

    @ApiProperty({
        description: 'Distribution Chart Values',
        isArray: true,
        type: Number,
    })
        data: number[];
}

/**
 * The evolution chart type describes the data as requested by DAPP charting library
 * Any evolution query result can be transformed into this data type using the transformation service.
 */
export class EvolutionChartSegment {
    @ApiProperty({
        description: 'Segment Name',
    })
        name: string;

    @ApiProperty({
        description: 'Segment Data Points',
        type: Number,
        isArray: true,
    })
        data: number[];

}

/**
 * An evolution chart is an Array of Evolution segments that share the same labels. For example Evolution of several
 * parameters across eras.
 */
export class EvolutionChart {
    @ApiProperty({
        description: 'Segment Data Points',
        type: EvolutionChartSegment,
        isArray: true,
    })
        segments: Array<EvolutionChartSegment>;

    @ApiProperty({
        description: 'Segment Labels',
        type: Number,
        isArray: true,
    })
        labels: number[];
}

/**
 * Treemap chart. Any distribution can be transformed into a treemap chart.
 * A Treemap is an array of Segments and the Segment an Array of points.
 */

export class TreemapPoint {
    @ApiProperty({
        description: 'Point Label',
    })
        x: string;

    @ApiProperty({
        description: 'Point Value',
    })
        y: number;
}

export class TreemapSegment {
    @ApiProperty({
        description: 'Segment Name',
    })
        name: string;

    @ApiProperty({
        description: 'Segment Data',
        type: TreemapPoint,
        isArray: true,
    })
        data: Array<TreemapPoint>;
}

export type TreemapChart = Array<TreemapSegment>;

/**
 * Geographic Region Bundle. Packs all required responses for Geographic Regional Evolution
 */
export class GeoRegionOverview {

    @ApiProperty({
        type: DistributionChart,
    })
        topRegionalDistributionChart: DistributionChart;

    @ApiProperty({
        type: EvolutionChart,
        isArray: false,
    })
        regionalEvolutionChart: EvolutionChart;

    @ApiProperty({
        type: RewardsByRegion,
        isArray: true,
    })
        regionalDistributionDetail: Array<RewardsByRegion>;
}

/**
 * Network Overview bundle
 */
export class NetworkOverview {

    @ApiProperty({
        type: TreemapSegment,
        isArray: true,
    })
        topNetworkDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: RewardsByNetworkProvider,
        isArray: true,
    })
        networkDistributionDetail: Array<RewardsByNetworkProvider>;

}

/**
 * Validator Group / Operator overview data bundle.
 */

export class OperatorOverview {

    @ApiProperty({
        type: TreemapSegment,
    })
        topOperatorDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: RewardsByValidationGroup,
        isArray: true,
    })
        operatorDistributionDetail: Array<RewardsByValidationGroup>;

}

/**
 * Pool Group / Operator overview data bundle.
 */

export class PoolOverview {

    @ApiProperty({
        type: DistributionChart,
    })
        nominatorTypeDistributionChart: DistributionChart;

    @ApiProperty({
        type: TreemapSegment,
    })
        topPoolOperatorDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: RewardsByPoolGroup,
        isArray: true,
    })
        poolOperatorDistributionDetail: Array<RewardsByPoolGroup>;

}

/**
 * Detail information about a Region Data Bundle.
 */

export class RegionDetail {
    @ApiProperty({
        type: TreemapSegment,
    })
        topCountryDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: RewardsByCountry,
        isArray: true,
    })
        countryDistributionDetail: Array<RewardsByCountry>;

    @ApiProperty({
        type: TreemapSegment,
    })
        topOperatorDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: RewardsByValidationGroup,
        isArray: true,
    })
        operatorDistributionDetail: Array<RewardsByValidationGroup>;
}

/**
 * Country detail data bundle
 */
export class CountryDetail {
    @ApiProperty({
        type: TreemapSegment,
    })
        topNetworkDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: TreemapSegment,
    })
        topOperatorDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: RewardsByValidationGroup,
        isArray: true,
    })
        operatorDistributionDetail: Array<RewardsByValidationGroup>;
}

/**
 * Network Detail Data Bundle
 */
export class NetworkDetail {

    @ApiProperty({
        type: TreemapSegment,
    })
        topCountryDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: TreemapSegment,
    })
        topOperatorDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: RewardsByValidationGroup,
        isArray: true,
    })
        operatorDistributionDetail: Array<RewardsByValidationGroup>;

}

/**
 * Operator Detail Data Bundle
 */
export class OperatorDetail {

    @ApiProperty({
        type: DistributionChart,
    })
        topCountryDistributionChart: DistributionChart;

    @ApiProperty({
        type: DistributionChart,
    })
        topNetworkDistributionChart: DistributionChart;

    @ApiProperty({
        type: RewardsByValidationNode,
        isArray: true,
    })
        nodeDistributionDetail: Array<RewardsByValidationNode>;

}

/**
 * Validator Node Detail Data Bundle
 */
export class ValidatorDetail {

    @ApiProperty({
        type: DistributionChart,
    })
        topCountryDistributionChart: DistributionChart;

    @ApiProperty({
        type: DistributionChart,
    })
        topNetworkDistributionChart: DistributionChart;

    @ApiProperty({
        type: RewardsByValidationNode,
    })
        nodeDetail: RewardsByValidationNode;

}

/**
 * Pool Operator Detail Data Bundle
 */
export class PoolOperatorDetail {

    @ApiProperty({
        type: DistributionChart,
    })
        topRegionalDistributionChart: DistributionChart;

    @ApiProperty({
        type: DistributionChart,
    })
        topCountryDistributionChart: DistributionChart;

    @ApiProperty({
        type: DistributionChart,
    })
        topNetworkDistributionChart: DistributionChart;

    @ApiProperty({
        type: TreemapSegment,
    })
        topPoolInstanceDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: RewardsByPoolGroup,
        isArray: true,
    })
        poolInstanceDistributionDetail: Array<RewardsByPool>;

}

/**
 * Pool Operator Detail Data Bundle
 */
export class PoolInstanceDetail {

    @ApiProperty({
        type: DistributionChart,
    })
        topRegionalDistributionChart: DistributionChart;

    @ApiProperty({
        type: DistributionChart,
    })
        topCountryDistributionChart: DistributionChart;

    @ApiProperty({
        type: DistributionChart,
    })
        topNetworkDistributionChart: DistributionChart;

    @ApiProperty({
        type: TreemapSegment,
    })
        topOperatorDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: RewardsByValidationNode,
        isArray: true,
    })
        nodeDistributionDetail: Array<RewardsByValidationNode>;

}


/**
 * Nominator Detail Data Bundle
 */
export class NominatorDetail {

    @ApiProperty({
        type: DistributionChart,
    })
        topRegionalDistributionChart: DistributionChart;

    @ApiProperty({
        type: DistributionChart,
    })
        topCountryDistributionChart: DistributionChart;

    @ApiProperty({
        type: DistributionChart,
    })
        topNetworkDistributionChart: DistributionChart;

    @ApiProperty({
        type: TreemapSegment,
    })
        topOperatorDistributionChart: Array<TreemapSegment>;

    @ApiProperty({
        type: RewardsByValidationNode,
        isArray: true,
    })
        nodeDistributionDetail: Array<RewardsByValidationNode>;
}