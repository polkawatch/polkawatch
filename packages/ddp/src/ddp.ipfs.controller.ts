// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';

import {
    DdpLqsService,
    InventoryRecord,
} from './ddp.lqs.service';
import { DdpTransformationService } from './ddp.transformations.service';
import {
    AboutData,
    AboutDataQuality,
    GeoRegionOverview,
    NetworkOverview,
    OperatorOverview,
    RegionDetail,
    CountryDetail,
    NetworkDetail,
    OperatorDetail,
    NominatorDetail,
    ChainMetadata,
    PoolOverview,
    PoolOperatorDetail,
    PoolInstanceDetail, ValidatorDetail,
} from './ddp.types';
import { InventoryQuery } from '@lqs/client';
import { DdpChainService } from './ddp.chain.service';


/**
 * Distributed Data Pack Controller.
 *
 * These endpoints match the IPFS file structure. It is used during development and as the source for
 * IPFS data pack generation.
 * Note that all the parameters are selected from a limited number of options, this makes it possible to generate
 * a big, but still manageable Data Pack, at the end of the day, there are so many queries that make statistical sense.
 *
 * In pre 1.0 all entrypoints would query times in eras, which in Polkadot matches one day. With the introduction of
 * multichain support, the new unit of times is days, which on different chains results in different number of eras.
 * Translastion from days to eras is required prior to quering LQS, chain metadata is used for that purpose.
 *
 */

@Controller()
@ApiTags('polkawatch')
export class DdpIpfs {

    constructor(
        private readonly lqs: DdpLqsService,
        private readonly transformer: DdpTransformationService,
        private readonly chain: DdpChainService,
    ) {
        // nothing
    }

    /**
     * Information about the blockchain.
     */
    @Get('/about/chain.json')
    @ApiOkResponse({ description: 'The information about the blockchain', type: ChainMetadata, isArray: false })
    async aboutChain():Promise<ChainMetadata> {
        return this.lqs.getAPI().about.chainMeta().then(r => r.data);
    }

    /**
     * Information about the dataset. On the one hand, we can check the last N days, and we can check if we focus on
     * public validation (open validators with an ID) or ALL validation, including custodial (100% commission) and
     * validators without an ID.
     *
     * @param last_days
     * @param validation_type
     */
    @Get('/about/dataset/:validation_type/:last_days.json')
    @ApiOperation({
        description: 'Returns information about the dataset of the last N days.',
    })
    @ApiOkResponse({ description: 'The information about the selected dataset', type: AboutData, isArray: false })
    @ApiParam({
        description: 'Available set of days to query',
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async aboutDataset(
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<AboutData> {
        const api = this.lqs.getAPI();

        return (await api.about.aboutDatasetPost({
            aboutDataQuery: await this.getCommonRequestParameters({ last_days, validation_type }),
        })).data;
    }

    /**
     * Information about the data quality. On the one hand, we can check the last N days, and we can check if we focus on
     * public validation (open validators with an ID) or ALL validation, including custodial (100% commission) and
     * validators without an ID.
     *
     * @param last_days
     * @param validation_type
     */
    @Get('/about/datasetquality/:validation_type/:last_days.json')
    @ApiOperation({
        description: 'Returns information about the dataset quality of the last N days.',
    })
    @ApiOkResponse({ description: 'The information about the dataset quality', type: AboutDataQuality, isArray: false })
    @ApiParam({
        description: 'Available set of days to query',
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async aboutDatasetQuality(
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<AboutDataQuality> {
        const api = this.lqs.getAPI();

        return (await api.about.aboutDatasetQualityPost({
            aboutDataQualityQuery: await this.getCommonRequestParameters({ last_days, validation_type }),
        })).data;
    }

    /**
     * Information about the dataset. Inventory of IDs of participating objects.
     * This is required for IPFS generation in order to build all possible populated endpoints and dump them into an
     * ipfs CAR archive.
     *
     * @param record_type
     */
    @Get('/about/inventory/:record_type.json')
    @ApiOperation({
        description: 'Identifiers of participating entities. Available only for 60 days and public validation.',
    })
    @ApiOkResponse({ description: 'The information about the selected dataset', type: InventoryRecord, isArray: true })
    @ApiParam({
        description: 'The record type to get Identifiers from',
        name:'record_type',
        enum:['region', 'country', 'network', 'operator', 'validator', 'pool_operator', 'pool', 'nominator'],
    })
    async aboutInventory(
        @Param('record_type') record_type,
    ): Promise<Array<InventoryRecord>> {
        const api = this.lqs.getAPI();

        const commonParams = await this.getCommonRequestParameters({
            last_days: 60,
            validation_type: this.chain.inventoryType(),
            top_results: 25000,
        });

        // different nomenclature in LQS :(
        if(record_type == 'operator') record_type = 'validator_group';
        if(record_type == 'pool_operator') record_type = 'pool_group';


        const query:InventoryQuery = {
            RecordType: record_type,
            ...commonParams,
        };

        return (await api.about.dataSetInventoryPost({
            inventoryQuery: query,
        })).data;
    }

    /**
     * Regional information bundle.
     *
     * Returns all the queries required to present regional status of the network in one single object. Top N distribution,
     * Top N evolution, and All Regions detail.
     *
     * @param last_days
     * @param validation_type
     * @param top_results
     *
     */
    @Get('/geography/overview/:validation_type/:last_days/:top_results.json')
    @ApiOkResponse({ description: 'Data bundle of regional data', type: GeoRegionOverview, isArray: false })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    @ApiParam({
        description: 'Number of top regions',
        name:'top_results',
        enum: [3, 4, 5],
    })
    async geoRegionOverview(
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
        @Param('top_results') top_results:number,
    ): Promise<GeoRegionOverview> {
        const api = this.lqs.getAPI();

        const distributionQuery = await this.getCommonRequestParameters({ last_days, validation_type, top_results: top_results });
        const detailQuery = { ... distributionQuery, TopResults: 10 };
        const evolutionQuery = distributionQuery;

        return {
            topRegionalDistributionChart: this.transformer.toDistributionChart((await api.geography.geoRegionPost({
                rewardDistributionQuery: distributionQuery,
            })).data, 'Region'),
            regionalEvolutionChart: this.transformer.toEvolutionChart((await api.geography.geoRegionEvolutionPost({
                evolutionQuery: evolutionQuery,
            })).data),
            regionalDistributionDetail: (await api.geography.geoRegionPost({
                rewardDistributionQuery: detailQuery,
            })).data,
        } as GeoRegionOverview;
    }

    /**
     * Network information bundle.
     *
     * Returns all the queries required to present status by operating network in one single object.
     *
     * @param last_days
     * @param validation_type
     *
     */
    @Get('/network/overview/:validation_type/:last_days.json')
    @ApiOkResponse({ description: 'Data bundle of operator network data', type: NetworkOverview, isArray: false })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async networkOverview(
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<NetworkOverview> {
        const api = this.lqs.getAPI();

        const detailQuery = await this.getCommonRequestParameters({ last_days, validation_type, top_results: 75 });

        return {
            topNetworkDistributionChart: this.transformer.toTreemapChart((await api.network.networkProviderPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'NetworkProvider'),
            networkDistributionDetail: (await api.network.networkProviderPost({
                rewardDistributionQuery: detailQuery,
            })).data,
        } as NetworkOverview;
    }

    /**
     * ValidationGroup/Operator information bundle.
     *
     * Returns all the queries required to present status by validator group or operator in one single object.
     *
     * @param last_days
     * @param validation_type
     *
     */
    @Get('/operator/overview/:validation_type/:last_days.json')
    @ApiOkResponse({ description: 'Data bundle of validation group/operator data', type: OperatorOverview, isArray: false })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async operatorOverview(
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<OperatorOverview> {
        const api = this.lqs.getAPI();

        const detailQuery = await this.getCommonRequestParameters({ last_days, validation_type, top_results: 200 });

        return {
            topOperatorDistributionChart: this.transformer.toTreemapChart((await api.validator.validatorGroupPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'ValidationGroup'),
            operatorDistributionDetail: (await api.validator.validatorGroupPost({
                rewardDistributionQuery: detailQuery,
            })).data,
        } as OperatorOverview;
    }


    /**
     * Pool information bundle.
     *
     * Returns all the queries required to present status by pool group or pool operator in one single object.
     *
     * @param last_days
     * @param validation_type
     *
     */
    @Get('/pool/overview/:validation_type/:last_days.json')
    @ApiOkResponse({ description: 'Data bundle of pool group/operator data', type: PoolOverview, isArray: false })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async poolOverview(
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<PoolOverview> {
        const api = this.lqs.getAPI();

        const distributionQuery = await this.getCommonRequestParameters({ last_days, validation_type, top_results: 200 });

        // we will only display pools with identity unless all validation is requested
        // note that this is not the same as including all participating pools when reporting about a validator, for example.
        distributionQuery['PoolIdentityType'] = validation_type == 'public' ? 'with identity' : 'all';

        return {
            nominatorTypeDistributionChart: this.transformer.toDistributionChart((await api.pool.nominatorTypePost({
                rewardDistributionQuery: {
                    ...distributionQuery,
                    // We cannot filter by this, or we only get pooled
                    PoolIdentityType: 'all',
                },
            })).data, 'NominatorType'),
            topPoolOperatorDistributionChart: this.transformer.toTreemapChart((await api.pool.poolGroupPost({
                rewardDistributionQuery: distributionQuery,
            })).data, 'PoolGroup'),
            poolOperatorDistributionDetail: (await api.pool.poolGroupPost({
                rewardDistributionQuery: distributionQuery,
            })).data,
        } as PoolOverview;
    }


    /**
     * Region Detail View. In depth view about a single region: i.e. Africa.
     */
    @Get('/geography/region/:region/:validation_type/:last_days.json')
    @ApiOkResponse({ description: 'Data bundle of region detail data', type: RegionDetail, isArray: false })
    @ApiParam({
        description: 'Region ID to request',
        name:'region',
    })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async regionDetail(
        @Param('region') region,
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<RegionDetail> {
        const api = this.lqs.getAPI();

        const commonParams = await this.getCommonRequestParameters({ last_days, validation_type:validation_type, top_results: 200 });
        const detailQuery = { RegionFilter: region, ... commonParams };

        return {
            topCountryDistributionChart: this.transformer.toTreemapChart((await api.geography.geoCountryPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'Country'),
            countryDistributionDetail: (await api.geography.geoCountryPost({
                rewardDistributionQuery: detailQuery,
            })).data,
            topOperatorDistributionChart: this.transformer.toTreemapChart((await api.validator.validatorGroupPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'ValidationGroup'),
            operatorDistributionDetail: (await api.validator.validatorGroupPost({
                rewardDistributionQuery: detailQuery,
            })).data,
        } as RegionDetail;
    }

    /**
     * Country Detail View
     */
    @Get('/geography/country/:country/:validation_type/:last_days.json')
    @ApiOkResponse({ description: 'Data bundle of country detail data', type: CountryDetail, isArray: false })
    @ApiParam({
        description: 'Country ID to request',
        name:'country',
    })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async countryDetail(
        @Param('country') country,
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<CountryDetail> {
        const api = this.lqs.getAPI();

        const commonParams = await this.getCommonRequestParameters({ last_days, validation_type:validation_type, top_results: 200 });
        const detailQuery = { CountryFilter: country, ... commonParams };

        return {
            topNetworkDistributionChart: this.transformer.toTreemapChart((await api.network.networkProviderPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'NetworkProvider'),
            topOperatorDistributionChart: this.transformer.toTreemapChart((await api.validator.validatorGroupPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'ValidationGroup'),
            operatorDistributionDetail: (await api.validator.validatorGroupPost({
                rewardDistributionQuery: detailQuery,
            })).data,
        } as CountryDetail;
    }

    /**
     * Network Detail View
     */
    @Get('/network/:network/:validation_type/:last_days.json')
    @ApiOkResponse({ description: 'Data bundle of network detail data', type: NetworkDetail, isArray: false })
    @ApiParam({
        description: 'Network ID to request',
        name:'network',
    })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async networkDetail(
        @Param('network') network,
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<NetworkDetail> {
        const api = this.lqs.getAPI();

        const commonParams = await this.getCommonRequestParameters({ last_days, validation_type:validation_type, top_results: 200 });
        const detailQuery = { NetworkFilter: network, ... commonParams };

        return {
            topCountryDistributionChart: this.transformer.toTreemapChart((await api.geography.geoCountryPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'Country'),
            topOperatorDistributionChart: this.transformer.toTreemapChart((await api.validator.validatorGroupPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'ValidationGroup'),
            operatorDistributionDetail: (await api.validator.validatorGroupPost({
                rewardDistributionQuery: detailQuery,
            })).data,
        } as NetworkDetail;
    }

    /**
     * Operator Detail View
     */
    @Get('/operator/:operator/:validation_type/:last_days.json')
    @ApiOkResponse({ description: 'Data bundle of operator detail data', type: OperatorDetail, isArray: false })
    @ApiParam({
        description: 'Operator ID to request',
        name:'operator',
    })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async operatorDetail(
        @Param('operator') operator,
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<OperatorDetail> {
        const api = this.lqs.getAPI();

        const commonParams = await this.getCommonRequestParameters({ last_days, validation_type:validation_type, top_results: 50 });
        const detailQuery = { ValidatorGroupFilter: operator, ... commonParams };

        return {
            topCountryDistributionChart: this.transformer.toDistributionChart((await api.geography.geoCountryPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'Country'),
            topNetworkDistributionChart: this.transformer.toDistributionChart((await api.network.networkProviderPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'NetworkProvider'),
            nodeDistributionDetail: (await api.validator.validatorNodePost({
                rewardDistributionQuery: detailQuery,
            })).data,
        } as OperatorDetail;
    }


    /**
     * Validator Node Detail View
     */
    @Get('/validator/:validator/:validation_type/:last_days.json')
    @ApiOkResponse({ description: 'Data bundle of validator detail data', type: ValidatorDetail, isArray: false })
    @ApiParam({
        description: 'Validator Node ID to request',
        name:'validator',
    })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async validatorDetail(
        @Param('validator') validator,
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<ValidatorDetail> {
        const api = this.lqs.getAPI();

        const commonParams = await this.getCommonRequestParameters({ last_days, validation_type:validation_type, top_results: 50 });
        const detailQuery = { ValidatorFilter: validator, ... commonParams };

        return {
            topCountryDistributionChart: this.transformer.toDistributionChart((await api.geography.geoCountryPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'Country'),
            topNetworkDistributionChart: this.transformer.toDistributionChart((await api.network.networkProviderPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'NetworkProvider'),
            nodeDetail: (await api.validator.validatorNodePost({
                rewardDistributionQuery: detailQuery,
            })).data[0],
        } as ValidatorDetail;
    }


    /**
     * Nominator Detail View, only available for public validation and last 30 days
     */
    @Get('/nominator/:nominator/:last_days.json')
    @ApiOkResponse({ description: 'Data bundle of nominator detail data', type: NominatorDetail, isArray: false })
    @ApiParam({
        description: 'Nominator ID to request',
        name:'nominator',
    })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30],
    })
    async nominatorDetail(
        @Param('nominator') nominator,
        @Param('last_days') last_days:number,
    ): Promise<NominatorDetail> {
        const api = this.lqs.getAPI();

        const commonParams = await this.getCommonRequestParameters({ last_days, validation_type:'public', top_results: 20 });
        const detailQuery = { NominatorFilter: nominator, ... commonParams };

        return {
            topRegionalDistributionChart:  this.transformer.toDistributionChart((await api.geography.geoRegionPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'Region'),
            topCountryDistributionChart: this.transformer.toDistributionChart((await api.geography.geoCountryPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'Country'),
            topNetworkDistributionChart: this.transformer.toDistributionChart((await api.network.networkProviderPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'NetworkProvider'),
            topOperatorDistributionChart:  this.transformer.toTreemapChart((await api.validator.validatorGroupPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'ValidationGroup'),
            nodeDistributionDetail: (await api.validator.validatorNodePost({
                rewardDistributionQuery: detailQuery,
            })).data,
        } as NominatorDetail;
    }


    /**
     * Pool Operator Detail View
     */
    @Get('/pool/operator/:pool_operator/:validation_type/:last_days.json')
    @ApiOkResponse({ description: 'Data bundle of pool operator detail data', type: PoolOperatorDetail, isArray: false })
    @ApiParam({
        description: 'Pool Operator ID to request',
        name:'pool_operator',
    })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async poolOperatorDetail(
        @Param('pool_operator') pool_operator,
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<PoolOperatorDetail> {
        const api = this.lqs.getAPI();

        const commonParams = await this.getCommonRequestParameters({ last_days, validation_type:validation_type, top_results: 50 });
        const detailQuery = { PoolGroupFilter: pool_operator, ... commonParams };

        return {
            topRegionalDistributionChart:  this.transformer.toDistributionChart((await api.geography.geoRegionPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'Region'),
            topCountryDistributionChart: this.transformer.toDistributionChart((await api.geography.geoCountryPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'Country'),
            topNetworkDistributionChart: this.transformer.toDistributionChart((await api.network.networkProviderPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'NetworkProvider'),

            topPoolInstanceDistributionChart: this.transformer.toTreemapChart((await api.pool.poolPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'Pool'),
            poolInstanceDistributionDetail: (await api.pool.poolPost({
                rewardDistributionQuery: detailQuery,
            })).data,
        } as PoolOperatorDetail;
    }

    /**
     * Pool Instance Detail View
     */
    @Get('/pool/instance/:pool/:validation_type/:last_days.json')
    @ApiOkResponse({ description: 'Data bundle of pool detail data', type: PoolInstanceDetail, isArray: false })
    @ApiParam({
        description: 'Pool ID to request',
        name:'pool',
    })
    @ApiParam({
        description: 'Available set of days to query',
        type: Number,
        name:'last_days',
        enum: [30, 60],
    })
    @ApiParam({
        description: 'Limit to Staking Rewards of Public Validators with Identity, Aura Authorities, or include ALL rewards and commissions from All validators/collators',
        name:'validation_type',
        enum:['public', 'authority', 'all'],
    })
    async poolDetail(
        @Param('pool') pool,
        @Param('last_days') last_days:number,
        @Param('validation_type') validation_type,
    ): Promise<PoolInstanceDetail> {
        const api = this.lqs.getAPI();

        const commonParams = await this.getCommonRequestParameters({ last_days, validation_type:validation_type, top_results: 50 });
        const detailQuery = { PoolFilter: pool, ... commonParams };

        return {
            topRegionalDistributionChart:  this.transformer.toDistributionChart((await api.geography.geoRegionPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'Region'),
            topCountryDistributionChart: this.transformer.toDistributionChart((await api.geography.geoCountryPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'Country'),
            topNetworkDistributionChart: this.transformer.toDistributionChart((await api.network.networkProviderPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'NetworkProvider'),
            topOperatorDistributionChart:  this.transformer.toTreemapChart((await api.validator.validatorGroupPost({
                rewardDistributionQuery: detailQuery,
            })).data, 'ValidationGroup'),
            nodeDistributionDetail: (await api.validator.validatorNodePost({
                rewardDistributionQuery: detailQuery,
            })).data,
        } as PoolInstanceDetail;
    }

    /**
     * Helper method to fill up convert shared request parameters to LQS request parameters
     */
    async getCommonRequestParameters(params) {
        const queryParams = {};
        if (params.last_days) queryParams['StartingEra'] = await this.lqs.getStartingEra(this.chain.daysToEras(params.last_days));
        if(params.validation_type) {
            queryParams['RewardType'] = params.validation_type == 'public' ? 'staking reward' : 'all';
            queryParams['ValidatorType'] = params.validation_type == 'public' ? 'public' : 'all';
            queryParams['ValidatorIdentityType'] = params.validation_type == 'public' ? 'with identity' : 'all';
        }
        if(params.top_results) queryParams['TopResults'] = params.top_results;
        return queryParams;
    }

}
