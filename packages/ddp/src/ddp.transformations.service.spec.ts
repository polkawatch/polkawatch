// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import * as Joi from 'joi';
import { ConfigModule } from '@nestjs/config';
import { DdpTransformationService } from './ddp.transformations.service';

describe('Transformation Service', () => {
    let transformationService: DdpTransformationService;

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    isGlobal: true,
                    validationSchema: Joi.object({
                        DDP_LQS_HOST: Joi.string().default('localhost'),
                        DDP_LQS_PROTO: Joi.string().default('http'),
                        DDP_LQS_PORT: Joi.number().default(7000),
                    }),
                }),
            ],
            controllers: [],
            providers: [DdpTransformationService],
        }).compile();

        transformationService = app.get<DdpTransformationService>(DdpTransformationService) as DdpTransformationService;
    });

    describe('transformation service', () => {
        it('the transformation service should be defined"', () => {
            expect(transformationService).toBeDefined();
        });

        it('Will transform a distribution to a chart', () => {
            const chart = transformationService.toDistributionChart(test_data.test_distribution, 'Region');
            expect(chart).toBeDefined();
            expect(chart.data.length).toBe(5);
            expect(chart.labels[0]).toBe('Europe');
        });

        it('Will transform a distribution to a Treemap', () => {
            const chart = transformationService.toTreemapChart(test_data.test_distribution, 'Region');
            expect(chart).toBeDefined();
            expect(chart[0].name).toBe('Region');
            expect(chart[0].data.length).toBe(5);
            expect(chart[0].data[0].x).toBe('Europe');
        });

        it('Will transform an evolution query to a Chart', () => {
            const chart = transformationService.toEvolutionChart(test_data.test_evolution);
            expect(chart).toBeDefined();
            expect(chart.labels.length).toBe(161);
            expect(chart.segments.length).toBe(6);
            expect(chart.segments[0].name).toBe('EU');
        });

    });
});

/**
 * Test Data for Transformation unit tests.
 * Update this data from LQS Swagger UI.
 */
const test_data = {
    test_distribution: [
        {
            'TokenRewards': 16695.61454999409,
            'Countries': 24,
            'Networks': 49,
            'ValidatorGroups': 294,
            'Validators': 650,
            'PoolGroups': 46,
            'Pools': 53,
            'Nominators': 7368,
            'Id': 'EU',
            'Region': 'Europe',
        },
        {
            'TokenRewards': 2283.99384503504,
            'Countries': 3,
            'Networks': 30,
            'ValidatorGroups': 132,
            'Validators': 170,
            'PoolGroups': 23,
            'Pools': 26,
            'Nominators': 3969,
            'Id': 'NA',
            'Region': 'North America',
        },
        {
            'TokenRewards': 459.059028800091,
            'Countries': 7,
            'Networks': 9,
            'ValidatorGroups': 15,
            'Validators': 31,
            'PoolGroups': 7,
            'Pools': 9,
            'Nominators': 1644,
            'Id': 'AS',
            'Region': 'Asia',
        },
        {
            'TokenRewards': 19.09351785342,
            'Countries': 1,
            'Networks': 1,
            'ValidatorGroups': 1,
            'Validators': 2,
            'PoolGroups': 0,
            'Pools': 0,
            'Nominators': 16,
            'Id': 'OC',
            'Region': 'Oceania',
        },
        {
            'TokenRewards': 7.251679351158,
            'Countries': 1,
            'Networks': 1,
            'ValidatorGroups': 1,
            'Validators': 1,
            'PoolGroups': 0,
            'Pools': 0,
            'Nominators': 40,
            'Id': 'SA',
            'Region': 'South America',
        },
    ],
    test_evolution: [
        {
            'Id': 'EU',
            'Segment': [
                {
                    'Era': 550,
                    'TokenRewards': 565.7091762447,
                },
                {
                    'Era': 551,
                    'TokenRewards': 561.9996662719,
                },
                {
                    'Era': 552,
                    'TokenRewards': 712.2420038096,
                },
                {
                    'Era': 553,
                    'TokenRewards': 401.103128228,
                },
                {
                    'Era': 554,
                    'TokenRewards': 536.1084893156,
                },
                {
                    'Era': 555,
                    'TokenRewards': 288.4595230769,
                },
                {
                    'Era': 556,
                    'TokenRewards': 494.6597020325,
                },
                {
                    'Era': 557,
                    'TokenRewards': 625.312547751,
                },
                {
                    'Era': 558,
                    'TokenRewards': 632.7006699389,
                },
                {
                    'Era': 559,
                    'TokenRewards': 631.0438134274,
                },
                {
                    'Era': 560,
                    'TokenRewards': 534.4905384247,
                },
                {
                    'Era': 561,
                    'TokenRewards': 130.0265301645,
                },
                {
                    'Era': 562,
                    'TokenRewards': 489.48312294560003,
                },
                {
                    'Era': 563,
                    'TokenRewards': 498.74539058040006,
                },
                {
                    'Era': 564,
                    'TokenRewards': 636.9508446101,
                },
                {
                    'Era': 565,
                    'TokenRewards': 943.4572958058,
                },
                {
                    'Era': 569,
                    'TokenRewards': 1413.4406759833,
                },
                {
                    'Era': 570,
                    'TokenRewards': 3882.4724642693,
                },
                {
                    'Era': 571,
                    'TokenRewards': 2962.7778167859,
                },
                {
                    'Era': 572,
                    'TokenRewards': 6886.7060272362005,
                },
                {
                    'Era': 573,
                    'TokenRewards': 60358.8509077903,
                },
                {
                    'Era': 574,
                    'TokenRewards': 60092.7569474085,
                },
                {
                    'Era': 575,
                    'TokenRewards': 60007.7247572919,
                },
                {
                    'Era': 576,
                    'TokenRewards': 61485.6854475707,
                },
                {
                    'Era': 577,
                    'TokenRewards': 59762.47060592,
                },
                {
                    'Era': 578,
                    'TokenRewards': 64253.937910329,
                },
                {
                    'Era': 579,
                    'TokenRewards': 65747.0926168183,
                },
                {
                    'Era': 580,
                    'TokenRewards': 71251.010067355,
                },
                {
                    'Era': 581,
                    'TokenRewards': 64633.6393149009,
                },
                {
                    'Era': 582,
                    'TokenRewards': 64525.4058683782,
                },
                {
                    'Era': 583,
                    'TokenRewards': 61891.0871323966,
                },
                {
                    'Era': 584,
                    'TokenRewards': 66802.25615445319,
                },
                {
                    'Era': 585,
                    'TokenRewards': 65919.13560711,
                },
                {
                    'Era': 586,
                    'TokenRewards': 67430.5977525149,
                },
                {
                    'Era': 587,
                    'TokenRewards': 66797.8127939855,
                },
                {
                    'Era': 588,
                    'TokenRewards': 69722.3855874131,
                },
                {
                    'Era': 589,
                    'TokenRewards': 68867.8522052854,
                },
                {
                    'Era': 590,
                    'TokenRewards': 65867.4676396391,
                },
                {
                    'Era': 591,
                    'TokenRewards': 66591.4771736844,
                },
                {
                    'Era': 592,
                    'TokenRewards': 67092.5749780083,
                },
                {
                    'Era': 593,
                    'TokenRewards': 63139.1581979907,
                },
                {
                    'Era': 594,
                    'TokenRewards': 58779.9843699831,
                },
                {
                    'Era': 595,
                    'TokenRewards': 60681.7696607645,
                },
                {
                    'Era': 596,
                    'TokenRewards': 60554.5159192444,
                },
                {
                    'Era': 597,
                    'TokenRewards': 63172.1869699416,
                },
                {
                    'Era': 598,
                    'TokenRewards': 58835.8537595971,
                },
                {
                    'Era': 599,
                    'TokenRewards': 65535.8194226583,
                },
                {
                    'Era': 600,
                    'TokenRewards': 63187.859199419,
                },
                {
                    'Era': 601,
                    'TokenRewards': 61553.8129895691,
                },
                {
                    'Era': 602,
                    'TokenRewards': 66692.6479360725,
                },
                {
                    'Era': 603,
                    'TokenRewards': 61736.2204411787,
                },
                {
                    'Era': 604,
                    'TokenRewards': 62903.0873517979,
                },
                {
                    'Era': 605,
                    'TokenRewards': 62198.072019864,
                },
                {
                    'Era': 606,
                    'TokenRewards': 63103.2214114399,
                },
                {
                    'Era': 607,
                    'TokenRewards': 60580.0745956437,
                },
                {
                    'Era': 608,
                    'TokenRewards': 64915.3451359777,
                },
                {
                    'Era': 609,
                    'TokenRewards': 65395.3687167664,
                },
                {
                    'Era': 610,
                    'TokenRewards': 68497.2156337865,
                },
                {
                    'Era': 611,
                    'TokenRewards': 66143.2114185165,
                },
                {
                    'Era': 612,
                    'TokenRewards': 65158.1055232415,
                },
                {
                    'Era': 613,
                    'TokenRewards': 67964.152704872,
                },
                {
                    'Era': 614,
                    'TokenRewards': 69651.8790236114,
                },
                {
                    'Era': 615,
                    'TokenRewards': 62438.464679338496,
                },
                {
                    'Era': 616,
                    'TokenRewards': 62435.0140341772,
                },
                {
                    'Era': 617,
                    'TokenRewards': 67367.0110585434,
                },
                {
                    'Era': 618,
                    'TokenRewards': 60760.3603159861,
                },
                {
                    'Era': 619,
                    'TokenRewards': 64117.2895349349,
                },
                {
                    'Era': 620,
                    'TokenRewards': 66859.4992377234,
                },
                {
                    'Era': 621,
                    'TokenRewards': 65350.3952354695,
                },
                {
                    'Era': 622,
                    'TokenRewards': 67238.73488469,
                },
                {
                    'Era': 623,
                    'TokenRewards': 65073.8282859496,
                },
                {
                    'Era': 624,
                    'TokenRewards': 67183.7890974225,
                },
                {
                    'Era': 625,
                    'TokenRewards': 67476.4933478727,
                },
                {
                    'Era': 626,
                    'TokenRewards': 66136.6464920175,
                },
                {
                    'Era': 627,
                    'TokenRewards': 66958.4948621178,
                },
                {
                    'Era': 628,
                    'TokenRewards': 66591.6679397063,
                },
                {
                    'Era': 629,
                    'TokenRewards': 68740.8064777672,
                },
                {
                    'Era': 630,
                    'TokenRewards': 64732.987565486,
                },
                {
                    'Era': 631,
                    'TokenRewards': 70411.6728985767,
                },
                {
                    'Era': 632,
                    'TokenRewards': 68904.9867990973,
                },
                {
                    'Era': 633,
                    'TokenRewards': 66673.2033127169,
                },
                {
                    'Era': 634,
                    'TokenRewards': 70886.014329817,
                },
                {
                    'Era': 635,
                    'TokenRewards': 70014.2592712155,
                },
                {
                    'Era': 636,
                    'TokenRewards': 64750.992980648596,
                },
                {
                    'Era': 637,
                    'TokenRewards': 70088.5668605093,
                },
                {
                    'Era': 638,
                    'TokenRewards': 64638.4852663456,
                },
                {
                    'Era': 639,
                    'TokenRewards': 62790.1764322636,
                },
                {
                    'Era': 640,
                    'TokenRewards': 60330.3925954196,
                },
                {
                    'Era': 641,
                    'TokenRewards': 67623.0797702342,
                },
                {
                    'Era': 642,
                    'TokenRewards': 62721.8410692061,
                },
                {
                    'Era': 643,
                    'TokenRewards': 66393.9232488604,
                },
                {
                    'Era': 644,
                    'TokenRewards': 63448.2379054553,
                },
                {
                    'Era': 645,
                    'TokenRewards': 62430.9490707709,
                },
                {
                    'Era': 646,
                    'TokenRewards': 59610.3041045203,
                },
                {
                    'Era': 647,
                    'TokenRewards': 60743.1564779393,
                },
                {
                    'Era': 648,
                    'TokenRewards': 62148.7552410181,
                },
                {
                    'Era': 649,
                    'TokenRewards': 60800.4849673715,
                },
                {
                    'Era': 650,
                    'TokenRewards': 63234.1303288073,
                },
                {
                    'Era': 651,
                    'TokenRewards': 60670.2219945829,
                },
                {
                    'Era': 652,
                    'TokenRewards': 60557.9144025444,
                },
                {
                    'Era': 653,
                    'TokenRewards': 62069.0365724782,
                },
                {
                    'Era': 654,
                    'TokenRewards': 60478.1003911488,
                },
                {
                    'Era': 655,
                    'TokenRewards': 58964.1855779354,
                },
                {
                    'Era': 656,
                    'TokenRewards': 65160.915016981104,
                },
                {
                    'Era': 657,
                    'TokenRewards': 66382.3684966918,
                },
                {
                    'Era': 658,
                    'TokenRewards': 64021.0640542057,
                },
                {
                    'Era': 659,
                    'TokenRewards': 59260.3482073086,
                },
                {
                    'Era': 660,
                    'TokenRewards': 53545.7997319671,
                },
                {
                    'Era': 661,
                    'TokenRewards': 56079.9462863194,
                },
                {
                    'Era': 662,
                    'TokenRewards': 63666.1258549063,
                },
                {
                    'Era': 663,
                    'TokenRewards': 62683.8411166326,
                },
                {
                    'Era': 664,
                    'TokenRewards': 60608.7612681452,
                },
                {
                    'Era': 665,
                    'TokenRewards': 65590.5663874397,
                },
                {
                    'Era': 666,
                    'TokenRewards': 60069.4788418123,
                },
                {
                    'Era': 667,
                    'TokenRewards': 64889.5625453129,
                },
                {
                    'Era': 668,
                    'TokenRewards': 62042.8961531924,
                },
                {
                    'Era': 669,
                    'TokenRewards': 61645.5665440281,
                },
                {
                    'Era': 670,
                    'TokenRewards': 62014.3406670573,
                },
                {
                    'Era': 671,
                    'TokenRewards': 56158.2227569161,
                },
                {
                    'Era': 672,
                    'TokenRewards': 62851.5819474843,
                },
                {
                    'Era': 673,
                    'TokenRewards': 58579.9442063509,
                },
                {
                    'Era': 674,
                    'TokenRewards': 59837.4308968996,
                },
                {
                    'Era': 675,
                    'TokenRewards': 55359.5264929553,
                },
                {
                    'Era': 676,
                    'TokenRewards': 56597.6665081873,
                },
                {
                    'Era': 677,
                    'TokenRewards': 61645.1010900985,
                },
                {
                    'Era': 678,
                    'TokenRewards': 59592.3726059362,
                },
                {
                    'Era': 679,
                    'TokenRewards': 64331.8907495264,
                },
                {
                    'Era': 680,
                    'TokenRewards': 64393.739395142,
                },
                {
                    'Era': 681,
                    'TokenRewards': 60377.2267035624,
                },
                {
                    'Era': 682,
                    'TokenRewards': 62751.7201595943,
                },
                {
                    'Era': 683,
                    'TokenRewards': 61768.7248638943,
                },
                {
                    'Era': 684,
                    'TokenRewards': 57569.3959382465,
                },
                {
                    'Era': 685,
                    'TokenRewards': 54453.127308903,
                },
            ],
        },
        {
            'Id': 'NA',
            'Segment': [
                {
                    'Era': 525,
                    'TokenRewards': 250.93831183449998,
                },
                {
                    'Era': 526,
                    'TokenRewards': 225.3444549847,
                },
                {
                    'Era': 527,
                    'TokenRewards': 212.7212309288,
                },
                {
                    'Era': 533,
                    'TokenRewards': 260.9908741475,
                },
                {
                    'Era': 534,
                    'TokenRewards': 176.06599980299998,
                },
                {
                    'Era': 535,
                    'TokenRewards': 255.2867677154,
                },
                {
                    'Era': 538,
                    'TokenRewards': 179.3772580944,
                },
                {
                    'Era': 539,
                    'TokenRewards': 235.9687899705,
                },
                {
                    'Era': 541,
                    'TokenRewards': 199.55260698220002,
                },
                {
                    'Era': 543,
                    'TokenRewards': 212.07073081029998,
                },
                {
                    'Era': 548,
                    'TokenRewards': 268.4980440391,
                },
                {
                    'Era': 551,
                    'TokenRewards': 236.3361471654,
                },
                {
                    'Era': 552,
                    'TokenRewards': 230.7961595738,
                },
                {
                    'Era': 553,
                    'TokenRewards': 174.83318984009998,
                },
                {
                    'Era': 557,
                    'TokenRewards': 182.4679952269,
                },
                {
                    'Era': 558,
                    'TokenRewards': 318.5852855839,
                },
                {
                    'Era': 559,
                    'TokenRewards': 537.1277381891,
                },
                {
                    'Era': 560,
                    'TokenRewards': 489.39273043220004,
                },
                {
                    'Era': 562,
                    'TokenRewards': 480.3835962211,
                },
                {
                    'Era': 563,
                    'TokenRewards': 586.1039349022,
                },
                {
                    'Era': 568,
                    'TokenRewards': 766.6112980409999,
                },
                {
                    'Era': 570,
                    'TokenRewards': 616.7947512294,
                },
                {
                    'Era': 571,
                    'TokenRewards': 2128.8600510538,
                },
                {
                    'Era': 572,
                    'TokenRewards': 1365.2510021950002,
                },
                {
                    'Era': 573,
                    'TokenRewards': 15377.7943149351,
                },
                {
                    'Era': 574,
                    'TokenRewards': 13885.431369206899,
                },
                {
                    'Era': 575,
                    'TokenRewards': 15431.3742445398,
                },
                {
                    'Era': 576,
                    'TokenRewards': 15279.5717164684,
                },
                {
                    'Era': 577,
                    'TokenRewards': 14969.4149396928,
                },
                {
                    'Era': 578,
                    'TokenRewards': 12682.8962729591,
                },
                {
                    'Era': 579,
                    'TokenRewards': 14508.2953472166,
                },
                {
                    'Era': 580,
                    'TokenRewards': 10816.1319063629,
                },
                {
                    'Era': 581,
                    'TokenRewards': 13298.9755070185,
                },
                {
                    'Era': 582,
                    'TokenRewards': 14136.494966427601,
                },
                {
                    'Era': 583,
                    'TokenRewards': 13011.8156459817,
                },
                {
                    'Era': 584,
                    'TokenRewards': 13635.9081206642,
                },
                {
                    'Era': 585,
                    'TokenRewards': 13948.8061379794,
                },
                {
                    'Era': 586,
                    'TokenRewards': 11963.0832855203,
                },
                {
                    'Era': 587,
                    'TokenRewards': 10511.6119613167,
                },
                {
                    'Era': 588,
                    'TokenRewards': 11335.3638713308,
                },
                {
                    'Era': 589,
                    'TokenRewards': 12082.747910690401,
                },
                {
                    'Era': 590,
                    'TokenRewards': 10538.1851981319,
                },
                {
                    'Era': 591,
                    'TokenRewards': 11434.1980226309,
                },
                {
                    'Era': 592,
                    'TokenRewards': 11735.4420906172,
                },
                {
                    'Era': 593,
                    'TokenRewards': 14242.637364505,
                },
                {
                    'Era': 594,
                    'TokenRewards': 18266.8539977172,
                },
                {
                    'Era': 595,
                    'TokenRewards': 16516.4483658348,
                },
                {
                    'Era': 596,
                    'TokenRewards': 16210.0788892702,
                },
                {
                    'Era': 597,
                    'TokenRewards': 15186.691947548401,
                },
                {
                    'Era': 598,
                    'TokenRewards': 14959.004203549499,
                },
                {
                    'Era': 599,
                    'TokenRewards': 16305.3237388285,
                },
                {
                    'Era': 600,
                    'TokenRewards': 16514.1020075052,
                },
                {
                    'Era': 601,
                    'TokenRewards': 16742.145962294,
                },
                {
                    'Era': 602,
                    'TokenRewards': 17173.6243188893,
                },
                {
                    'Era': 603,
                    'TokenRewards': 17060.8301724189,
                },
                {
                    'Era': 604,
                    'TokenRewards': 16479.0237248773,
                },
                {
                    'Era': 605,
                    'TokenRewards': 14682.7584668044,
                },
                {
                    'Era': 606,
                    'TokenRewards': 16650.4981423477,
                },
                {
                    'Era': 607,
                    'TokenRewards': 13856.7863320639,
                },
                {
                    'Era': 608,
                    'TokenRewards': 12637.9210015235,
                },
                {
                    'Era': 609,
                    'TokenRewards': 10303.4435136261,
                },
                {
                    'Era': 610,
                    'TokenRewards': 9717.919319263001,
                },
                {
                    'Era': 611,
                    'TokenRewards': 10602.2252573891,
                },
                {
                    'Era': 612,
                    'TokenRewards': 12833.2794567103,
                },
                {
                    'Era': 613,
                    'TokenRewards': 11498.4886967684,
                },
                {
                    'Era': 614,
                    'TokenRewards': 12140.4430641158,
                },
                {
                    'Era': 615,
                    'TokenRewards': 13329.8305376895,
                },
                {
                    'Era': 616,
                    'TokenRewards': 15550.8983872842,
                },
                {
                    'Era': 617,
                    'TokenRewards': 14114.3658275791,
                },
                {
                    'Era': 618,
                    'TokenRewards': 14855.2697074161,
                },
                {
                    'Era': 619,
                    'TokenRewards': 11658.2004974985,
                },
                {
                    'Era': 620,
                    'TokenRewards': 11685.5485782607,
                },
                {
                    'Era': 621,
                    'TokenRewards': 10206.1460096628,
                },
                {
                    'Era': 622,
                    'TokenRewards': 12332.3473697044,
                },
                {
                    'Era': 623,
                    'TokenRewards': 10349.947627695401,
                },
                {
                    'Era': 624,
                    'TokenRewards': 8115.3900957754,
                },
                {
                    'Era': 625,
                    'TokenRewards': 11247.2525555808,
                },
                {
                    'Era': 626,
                    'TokenRewards': 11557.4942545019,
                },
                {
                    'Era': 627,
                    'TokenRewards': 11441.8404732504,
                },
                {
                    'Era': 628,
                    'TokenRewards': 16014.9060304113,
                },
                {
                    'Era': 629,
                    'TokenRewards': 12070.8118155147,
                },
                {
                    'Era': 630,
                    'TokenRewards': 11938.7211966477,
                },
                {
                    'Era': 631,
                    'TokenRewards': 9963.6343791335,
                },
                {
                    'Era': 632,
                    'TokenRewards': 12260.4886894082,
                },
                {
                    'Era': 633,
                    'TokenRewards': 12233.5886522162,
                },
                {
                    'Era': 634,
                    'TokenRewards': 13128.5493115734,
                },
                {
                    'Era': 635,
                    'TokenRewards': 14082.0516613815,
                },
                {
                    'Era': 636,
                    'TokenRewards': 14649.8419563177,
                },
                {
                    'Era': 637,
                    'TokenRewards': 12608.0211770752,
                },
                {
                    'Era': 638,
                    'TokenRewards': 16675.9820123837,
                },
                {
                    'Era': 639,
                    'TokenRewards': 17678.7470762587,
                },
                {
                    'Era': 640,
                    'TokenRewards': 19731.6174536764,
                },
                {
                    'Era': 641,
                    'TokenRewards': 13926.2088174215,
                },
                {
                    'Era': 642,
                    'TokenRewards': 12715.5552103329,
                },
                {
                    'Era': 643,
                    'TokenRewards': 13793.1389097975,
                },
                {
                    'Era': 644,
                    'TokenRewards': 13800.649433565799,
                },
                {
                    'Era': 645,
                    'TokenRewards': 13921.762004183,
                },
                {
                    'Era': 646,
                    'TokenRewards': 14560.3886003954,
                },
                {
                    'Era': 647,
                    'TokenRewards': 18589.8951629018,
                },
                {
                    'Era': 648,
                    'TokenRewards': 11846.1698710953,
                },
                {
                    'Era': 649,
                    'TokenRewards': 15233.4247508517,
                },
                {
                    'Era': 650,
                    'TokenRewards': 10868.2857319371,
                },
                {
                    'Era': 651,
                    'TokenRewards': 11997.1316196154,
                },
                {
                    'Era': 652,
                    'TokenRewards': 12581.0246708183,
                },
                {
                    'Era': 653,
                    'TokenRewards': 12237.1341871204,
                },
                {
                    'Era': 654,
                    'TokenRewards': 13793.1128179432,
                },
                {
                    'Era': 655,
                    'TokenRewards': 12527.1293194819,
                },
                {
                    'Era': 656,
                    'TokenRewards': 10272.3272530446,
                },
                {
                    'Era': 657,
                    'TokenRewards': 12043.4735293616,
                },
                {
                    'Era': 658,
                    'TokenRewards': 12556.2641067858,
                },
                {
                    'Era': 659,
                    'TokenRewards': 15055.4694806238,
                },
                {
                    'Era': 660,
                    'TokenRewards': 15706.324230296499,
                },
                {
                    'Era': 661,
                    'TokenRewards': 16535.9388882744,
                },
                {
                    'Era': 662,
                    'TokenRewards': 14463.2759223191,
                },
                {
                    'Era': 663,
                    'TokenRewards': 11915.0972479832,
                },
                {
                    'Era': 664,
                    'TokenRewards': 13932.0923622386,
                },
                {
                    'Era': 665,
                    'TokenRewards': 10808.631806220701,
                },
                {
                    'Era': 666,
                    'TokenRewards': 13821.5070781862,
                },
                {
                    'Era': 667,
                    'TokenRewards': 11083.9554202359,
                },
                {
                    'Era': 668,
                    'TokenRewards': 9547.5547398329,
                },
                {
                    'Era': 669,
                    'TokenRewards': 14139.198508045,
                },
                {
                    'Era': 670,
                    'TokenRewards': 14572.6014667242,
                },
                {
                    'Era': 671,
                    'TokenRewards': 14875.9508969469,
                },
                {
                    'Era': 672,
                    'TokenRewards': 12183.8872169502,
                },
                {
                    'Era': 673,
                    'TokenRewards': 14220.3118678784,
                },
                {
                    'Era': 674,
                    'TokenRewards': 13002.2249963622,
                },
                {
                    'Era': 675,
                    'TokenRewards': 13296.4876029096,
                },
                {
                    'Era': 676,
                    'TokenRewards': 12523.2470684281,
                },
                {
                    'Era': 677,
                    'TokenRewards': 12918.4756342614,
                },
                {
                    'Era': 678,
                    'TokenRewards': 10083.312938455,
                },
                {
                    'Era': 679,
                    'TokenRewards': 12591.276779583999,
                },
                {
                    'Era': 680,
                    'TokenRewards': 11261.9159954979,
                },
                {
                    'Era': 681,
                    'TokenRewards': 14871.5510041191,
                },
                {
                    'Era': 682,
                    'TokenRewards': 11783.0910490061,
                },
                {
                    'Era': 683,
                    'TokenRewards': 11971.8320192853,
                },
                {
                    'Era': 684,
                    'TokenRewards': 13358.3915982493,
                },
                {
                    'Era': 685,
                    'TokenRewards': 9850.4866618035,
                },
            ],
        },
        {
            'Id': 'AS',
            'Segment': [
                {
                    'Era': 570,
                    'TokenRewards': 2937.2002229897003,
                },
                {
                    'Era': 571,
                    'TokenRewards': 2179.8781211392,
                },
                {
                    'Era': 572,
                    'TokenRewards': 3196.3513975672,
                },
                {
                    'Era': 573,
                    'TokenRewards': 5637.7465524778,
                },
                {
                    'Era': 574,
                    'TokenRewards': 5001.4373761263,
                },
                {
                    'Era': 575,
                    'TokenRewards': 6138.0345933628,
                },
                {
                    'Era': 576,
                    'TokenRewards': 5461.5815263415,
                },
                {
                    'Era': 577,
                    'TokenRewards': 5975.4372560863,
                },
                {
                    'Era': 578,
                    'TokenRewards': 4952.1799667446,
                },
                {
                    'Era': 579,
                    'TokenRewards': 5277.2235583432,
                },
                {
                    'Era': 580,
                    'TokenRewards': 4756.2807816373,
                },
                {
                    'Era': 581,
                    'TokenRewards': 4541.6746423023,
                },
                {
                    'Era': 582,
                    'TokenRewards': 5612.2614219263,
                },
                {
                    'Era': 583,
                    'TokenRewards': 4953.2507762661,
                },
                {
                    'Era': 584,
                    'TokenRewards': 4527.3507233756,
                },
                {
                    'Era': 585,
                    'TokenRewards': 5078.246684011,
                },
                {
                    'Era': 586,
                    'TokenRewards': 4550.9773695853,
                },
                {
                    'Era': 587,
                    'TokenRewards': 4872.8311064104,
                },
                {
                    'Era': 588,
                    'TokenRewards': 4522.5747617744,
                },
                {
                    'Era': 589,
                    'TokenRewards': 4479.2886991188,
                },
                {
                    'Era': 590,
                    'TokenRewards': 5083.3556941484,
                },
                {
                    'Era': 591,
                    'TokenRewards': 5013.1788952151,
                },
                {
                    'Era': 592,
                    'TokenRewards': 5017.3552289953,
                },
                {
                    'Era': 593,
                    'TokenRewards': 4490.0742432804,
                },
                {
                    'Era': 594,
                    'TokenRewards': 4261.309207804,
                },
                {
                    'Era': 595,
                    'TokenRewards': 5827.6038715661,
                },
                {
                    'Era': 596,
                    'TokenRewards': 4918.9668231705,
                },
                {
                    'Era': 597,
                    'TokenRewards': 7143.4874665625,
                },
                {
                    'Era': 598,
                    'TokenRewards': 6176.219252720501,
                },
                {
                    'Era': 599,
                    'TokenRewards': 3325.6925909462,
                },
                {
                    'Era': 600,
                    'TokenRewards': 5707.3600397184,
                },
                {
                    'Era': 601,
                    'TokenRewards': 5172.8847783806,
                },
                {
                    'Era': 602,
                    'TokenRewards': 4831.9063947517,
                },
                {
                    'Era': 603,
                    'TokenRewards': 3408.8661935723,
                },
                {
                    'Era': 604,
                    'TokenRewards': 4448.5702322306,
                },
                {
                    'Era': 605,
                    'TokenRewards': 5231.1456177873,
                },
                {
                    'Era': 606,
                    'TokenRewards': 4516.5936797052,
                },
                {
                    'Era': 607,
                    'TokenRewards': 6102.6458803538,
                },
                {
                    'Era': 608,
                    'TokenRewards': 5007.2487179701,
                },
                {
                    'Era': 609,
                    'TokenRewards': 5709.6834416617,
                },
                {
                    'Era': 610,
                    'TokenRewards': 4944.7476752853,
                },
                {
                    'Era': 611,
                    'TokenRewards': 5233.7170704342,
                },
                {
                    'Era': 612,
                    'TokenRewards': 4941.30444874,
                },
                {
                    'Era': 613,
                    'TokenRewards': 4141.7643134674,
                },
                {
                    'Era': 614,
                    'TokenRewards': 5337.4151964992,
                },
                {
                    'Era': 615,
                    'TokenRewards': 4605.597802999,
                },
                {
                    'Era': 616,
                    'TokenRewards': 4249.1328939661,
                },
                {
                    'Era': 617,
                    'TokenRewards': 4992.1616192673,
                },
                {
                    'Era': 618,
                    'TokenRewards': 4948.7995279996,
                },
                {
                    'Era': 619,
                    'TokenRewards': 5010.486443163,
                },
                {
                    'Era': 620,
                    'TokenRewards': 4914.0328655724,
                },
                {
                    'Era': 621,
                    'TokenRewards': 4992.8673823522,
                },
                {
                    'Era': 622,
                    'TokenRewards': 5607.1563767751,
                },
                {
                    'Era': 623,
                    'TokenRewards': 4360.9699466408,
                },
                {
                    'Era': 624,
                    'TokenRewards': 4170.4626829096,
                },
                {
                    'Era': 625,
                    'TokenRewards': 5469.921091854,
                },
                {
                    'Era': 626,
                    'TokenRewards': 4788.4922340681005,
                },
                {
                    'Era': 627,
                    'TokenRewards': 5286.8092412467,
                },
                {
                    'Era': 628,
                    'TokenRewards': 5532.9897152417,
                },
                {
                    'Era': 629,
                    'TokenRewards': 4687.3187160359,
                },
                {
                    'Era': 630,
                    'TokenRewards': 6776.8518340019,
                },
                {
                    'Era': 631,
                    'TokenRewards': 5248.1538639835,
                },
                {
                    'Era': 632,
                    'TokenRewards': 5777.551531087101,
                },
                {
                    'Era': 633,
                    'TokenRewards': 3730.0879074896,
                },
                {
                    'Era': 634,
                    'TokenRewards': 4529.8944436102,
                },
                {
                    'Era': 635,
                    'TokenRewards': 5386.477475427901,
                },
                {
                    'Era': 636,
                    'TokenRewards': 5148.4372167733,
                },
                {
                    'Era': 637,
                    'TokenRewards': 6005.7787222567,
                },
                {
                    'Era': 638,
                    'TokenRewards': 5040.6576127977005,
                },
                {
                    'Era': 639,
                    'TokenRewards': 4343.8178399173,
                },
                {
                    'Era': 640,
                    'TokenRewards': 6237.9142672498,
                },
                {
                    'Era': 641,
                    'TokenRewards': 4117.0263825636,
                },
                {
                    'Era': 642,
                    'TokenRewards': 5645.6531511952,
                },
                {
                    'Era': 643,
                    'TokenRewards': 5678.1785348673,
                },
                {
                    'Era': 644,
                    'TokenRewards': 4864.3911318679,
                },
                {
                    'Era': 645,
                    'TokenRewards': 5950.6954314566,
                },
                {
                    'Era': 646,
                    'TokenRewards': 4934.5517120757,
                },
                {
                    'Era': 647,
                    'TokenRewards': 4339.6918609432,
                },
                {
                    'Era': 648,
                    'TokenRewards': 7155.0826250984,
                },
                {
                    'Era': 649,
                    'TokenRewards': 6234.3720909349,
                },
                {
                    'Era': 650,
                    'TokenRewards': 6464.6085179844,
                },
                {
                    'Era': 651,
                    'TokenRewards': 6467.8210676298,
                },
                {
                    'Era': 652,
                    'TokenRewards': 5428.8624106014995,
                },
                {
                    'Era': 653,
                    'TokenRewards': 5288.8273824879,
                },
                {
                    'Era': 654,
                    'TokenRewards': 4412.7134603066,
                },
                {
                    'Era': 655,
                    'TokenRewards': 5260.0895285517,
                },
                {
                    'Era': 656,
                    'TokenRewards': 5595.5797010105,
                },
                {
                    'Era': 657,
                    'TokenRewards': 4084.352884693,
                },
                {
                    'Era': 658,
                    'TokenRewards': 6276.7633995819,
                },
                {
                    'Era': 659,
                    'TokenRewards': 5484.1063092682,
                },
                {
                    'Era': 660,
                    'TokenRewards': 5458.9748346141,
                },
                {
                    'Era': 661,
                    'TokenRewards': 5790.0931845958,
                },
                {
                    'Era': 662,
                    'TokenRewards': 4512.1673984382,
                },
                {
                    'Era': 663,
                    'TokenRewards': 4393.3764655393,
                },
                {
                    'Era': 664,
                    'TokenRewards': 6516.71228795,
                },
                {
                    'Era': 665,
                    'TokenRewards': 8092.9939041296,
                },
                {
                    'Era': 666,
                    'TokenRewards': 6730.0633263573,
                },
                {
                    'Era': 667,
                    'TokenRewards': 6354.9244239031,
                },
                {
                    'Era': 668,
                    'TokenRewards': 7049.5804689237,
                },
                {
                    'Era': 669,
                    'TokenRewards': 5937.4905346226,
                },
                {
                    'Era': 670,
                    'TokenRewards': 5242.9887176303,
                },
                {
                    'Era': 671,
                    'TokenRewards': 4879.3777838774,
                },
                {
                    'Era': 672,
                    'TokenRewards': 5484.8733984226,
                },
                {
                    'Era': 673,
                    'TokenRewards': 7090.9560172166,
                },
                {
                    'Era': 674,
                    'TokenRewards': 5137.1643434991,
                },
                {
                    'Era': 675,
                    'TokenRewards': 5178.7762958529,
                },
                {
                    'Era': 676,
                    'TokenRewards': 6582.535991263,
                },
                {
                    'Era': 677,
                    'TokenRewards': 6956.980254142401,
                },
                {
                    'Era': 678,
                    'TokenRewards': 7180.1426907247005,
                },
                {
                    'Era': 679,
                    'TokenRewards': 5109.2994669229,
                },
                {
                    'Era': 680,
                    'TokenRewards': 6860.0517651141,
                },
                {
                    'Era': 681,
                    'TokenRewards': 5215.9066739875,
                },
                {
                    'Era': 682,
                    'TokenRewards': 7462.837161848401,
                },
                {
                    'Era': 683,
                    'TokenRewards': 7235.8864781647,
                },
                {
                    'Era': 684,
                    'TokenRewards': 4352.1407933216,
                },
                {
                    'Era': 685,
                    'TokenRewards': 2152.3356453858,
                },
            ],
        },
        {
            'Id': 'SA',
            'Segment': [
                {
                    'Era': 637,
                    'TokenRewards': 629.8864227493,
                },
                {
                    'Era': 638,
                    'TokenRewards': 805.7469937521,
                },
                {
                    'Era': 648,
                    'TokenRewards': 885.2987432818999,
                },
                {
                    'Era': 649,
                    'TokenRewards': 977.4786031094,
                },
                {
                    'Era': 650,
                    'TokenRewards': 996.4698458819,
                },
                {
                    'Era': 651,
                    'TokenRewards': 421.2010023182,
                },
                {
                    'Era': 652,
                    'TokenRewards': 798.1504524948,
                },
                {
                    'Era': 653,
                    'TokenRewards': 1010.1494536065,
                },
                {
                    'Era': 654,
                    'TokenRewards': 621.7524788562,
                },
                {
                    'Era': 655,
                    'TokenRewards': 1219.976829706,
                },
            ],
        },
        {
            'Id': 'AF',
            'Segment': [
                {
                    'Era': 612,
                    'TokenRewards': 670.0365715825,
                },
                {
                    'Era': 613,
                    'TokenRewards': 504.73132984299997,
                },
                {
                    'Era': 614,
                    'TokenRewards': 883.2493007592001,
                },
                {
                    'Era': 615,
                    'TokenRewards': 742.3285691973,
                },
                {
                    'Era': 620,
                    'TokenRewards': 824.7810028009001,
                },
                {
                    'Era': 621,
                    'TokenRewards': 663.7940078382001,
                },
                {
                    'Era': 645,
                    'TokenRewards': 435.1229055438,
                },
                {
                    'Era': 646,
                    'TokenRewards': 1063.835912429,
                },
                {
                    'Era': 647,
                    'TokenRewards': 514.7028400539,
                },
                {
                    'Era': 648,
                    'TokenRewards': 572.3392325946,
                },
            ],
        },
        {
            'Id': 'OC',
            'Segment': [
                {
                    'Era': 658,
                    'TokenRewards': 977.6735658337999,
                },
                {
                    'Era': 659,
                    'TokenRewards': 1212.3300179992,
                },
                {
                    'Era': 660,
                    'TokenRewards': 653.2329386329001,
                },
                {
                    'Era': 662,
                    'TokenRewards': 626.6045763222,
                },
                {
                    'Era': 682,
                    'TokenRewards': 590.616133559,
                },
                {
                    'Era': 683,
                    'TokenRewards': 799.7631605579,
                },
                {
                    'Era': 684,
                    'TokenRewards': 616.0221816911001,
                },
            ],
        },
    ],
};