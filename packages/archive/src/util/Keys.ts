// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { isHex, u8aToHex } from '@polkadot/util';
import { decodeAddress, encodeAddress } from '@polkadot/keyring';

/**
 * Attempts to deduce the validator by authorityId which seems related to session keys.
 * This information is not always available, as session keys change over time.
 *
 * @param api The polkadot API
 * @param key The key to resolve
 * @param keyType The key type
 */
export async function substrateKeyOwner(api, key, keyType = '0x696d6f6e') {
    if(!isHex(key)) key = u8aToHex(decodeAddress(key));
    const network = api.consts.system.ss58Prefix;
    const owner = await api.query.session.keyOwner([keyType, key]);
    if(!owner.isEmpty && owner !== '') return encodeAddress(owner.toHex(), network);
}