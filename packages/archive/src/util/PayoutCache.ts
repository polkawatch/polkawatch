// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import LRU from 'lru-cache';

/**
 * LRU Cache to relate Rewards with Payouts
 */
const eraPayoutCache = new LRU({
    max:10,
});

export type Payout = {
    id: string
    blockNumber: bigint
    eventIndex: number
    era: bigint
    validatorId: string
}

/**
 * We need to track when a Payout event takes place in chain because the Reward event
 * happens right after it and subQuery does not call handlers in order. This allows us to disambiguate
 * when several Payouts take place in the same block.
 *
 * @param payout to cache
 */
export function cachePayout(payout: Payout) {
    let blockPayouts;
    if (eraPayoutCache.has(payout.blockNumber)) blockPayouts = eraPayoutCache.get(payout.blockNumber);
    else blockPayouts = {};
    // record this block payout
    blockPayouts[payout.eventIndex] = payout;
    // cache the data
    eraPayoutCache.set(payout.blockNumber, blockPayouts);
}

/**
 * From all the payouts cached we return the last one that took place before right before the provided eventId
 * @param block
 * @param eventIndex
 * @param logger
 */
export function getCachedPayout(block: bigint, eventIndex, logger = console) : Payout {
    if(eraPayoutCache.has(block)) {
        const blockPayouts = eraPayoutCache.get(block);
        const payoutAtEventsIds = Object.keys(blockPayouts).map(i=>parseInt(i));

        // filter out any payout event AFTER the event, sort, return the last one
        const eraId = payoutAtEventsIds
            .filter(eid => eid < eventIndex)
            .sort((a, b)=>a - b)
            .pop();

        return blockPayouts[eraId];
    } else {logger.error(`Expected payout in block ${block} before event ${eventIndex} is missing`);}
}