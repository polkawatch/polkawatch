// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { SubstrateEvent } from '@subql/types';
import { EraIndex, AccountId } from '@polkadot/types/interfaces';

import { Payout as PayoutRecord } from '../types';
import { getValidator } from './Heartbeat';

import { cachePayout } from '../util/PayoutCache';

/**
 * Key Reward data is split between the Reward and Payout events. We need to match both.
 * The following code assumes that the Rewards events follow its Payout event within the same
 * block.
 *
 * @param event
 */
export async function hanblePayout(event: SubstrateEvent): Promise<void> {
    const blockNum = event.block.block.header.number.toBigInt();
    const eventIdx = event.idx;
    const eventId = `${blockNum}-${eventIdx}`;

    const payout = new PayoutRecord (
        eventId,
    );

    const { event: { data: [eraId, validatorId] } } = event;

    payout.blockNumber = blockNum;
    payout.era = (eraId as EraIndex).toBigInt();
    payout.eventIndex = eventIdx;
    const validator = await getValidator((validatorId as AccountId).toString());
    validator.lastPayoutId = payout.id;
    await validator.save();
    payout.validatorId = validator.id;
    await payout.save();

    logger.info(`PayoutStarted: ${eventId} ref era ${payout.era} and ${validator.id}`);
    logger.debug(JSON.stringify(event.event));

    cachePayout(payout);

}

