// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { SubstrateEvent } from '@subql/types';

import { Heartbeat as SubstrateHeartbeat } from '@polkadot/types/interfaces/imOnline';

import { Heartbeat, Peer, Validator } from '../types';

import { substrateKeyOwner } from '../util/Keys';

export async function handleHeartbeat(event: SubstrateEvent): Promise<void> {
    const blockNum = event.block.block.header.number.toBigInt();
    const eventId = `${blockNum}-${event.idx}`;
    const heartbeat = new Heartbeat(
        eventId,
    );

    const shb = event.extrinsic.extrinsic.method.args[0] as SubstrateHeartbeat;

    heartbeat.blockNumber = blockNum;
    heartbeat.authorityId = event.event.data[0].toString();

    if(shb.networkState) {
        heartbeat.peerId = shb.networkState.peerId.toHex();
        heartbeat.externalAddresses = JSON.stringify(shb.networkState.externalAddresses.toHuman());
    } else {
        logger.warn(`HeartBeat ${heartbeat.id} without networking information.`);
        heartbeat.externalAddresses = '[]';
    }

    await heartbeat.save();

    /**
     * Here we are going to try to identify who is behind a heartbeat, this is easy to findout
     * at the top of the chain because session keys are recent. Old heartbeats may have been issued
     * with old session keys and finding out who is behind it would be complex, however, we assume that
     * the relation peerId to validatorId is more durable.
     */
    let peer = heartbeat.peerId ? await Peer.get(heartbeat.peerId) : undefined;
    if(!peer) {
        // We don't yet know the validator associated to this peerId
        const keyOwner = await substrateKeyOwner(api, heartbeat.authorityId);
        if(keyOwner) {

            // We managed to get the validatorId from substrate using session objects. This
            // Is the easiest but it works only reliably for the top of the chain.

            const validator = await getValidator(keyOwner);
            if(isNackedHB(heartbeat)) {
                logger.warn(`Nacked HB ${heartbeat.id} detected.`);
            } else{
                validator.lastHeartbeatId = heartbeat.id;
            }

            // Only if Addressing information was present.
            if(heartbeat.peerId) {
                peer = new Peer(heartbeat.peerId);
                peer.validatorId = validator.id;
                await peer.save();
                validator.lastPeerId = peer.id;
                logger.info(`Peer ${heartbeat.peerId} is Validator ${validator.id}`);
            }

            // In all cases save, could be new from authority.
            await validator.save();

            // We will also store the validator with the heartbeat when known
            heartbeat.validatorId = validator.id;
            await heartbeat.save();

            // The Polkawatch Indexer component will attempt to match the Heartbeat in 2nd stage
            // indexing using the PeerId, but we leave it at this stage.

        } else {logger.info(`AuthorityId ${heartbeat.authorityId} could not be resolved`);}
    } else {
        // The Peer-Validator relationship was already identified.
        // already linked to a validatorID
        heartbeat.validatorId = peer.validatorId;
        await heartbeat.save();
        const validator = await getValidator(peer.validatorId);
        if(isNackedHB(heartbeat)) {logger.warn(`Nacked HB ${heartbeat.id} detected from Validator ${validator.id}, ignoring`);} else {
            validator.lastHeartbeatId = heartbeat.id;
        }
        validator.lastPeerId = peer.id;
        await validator.save();

        logger.info(`Heartbeat from already known peer is Validator ${peer.validatorId}`);
    }

    logger.info('Heartbeat: ' + eventId);
}


/**
 * Returns or Creates a new Validator in the Store.
 */

export async function getValidator(id) {
    // Subquery seems to take care of existing entities when trying to create a new instance.
    // However we go for this pattern as it is used for other entities too.
    let v = await Validator.get(id);
    if(!v) v = new Validator(id);
    return v;
}

/**
 * Returns if a HB contains Addressing information or not
 */

function isNackedHB(heartbeat) {
    const addrs: Array<any> = JSON.parse(heartbeat.externalAddresses);
    return addrs.length == 0;
}