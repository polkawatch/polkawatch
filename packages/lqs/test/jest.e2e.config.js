/*
 * For a detailed explanation regarding each configuration property and type check, visit:
 * https://jestjs.io/docs/en/configuration.html
 */

module.exports = {

    collectCoverageFrom: [
        'packages/cli/src/**/*.ts',
        'packages/common/src/**/*.ts',
        'packages/contract-processors/src/**/*.ts',
        'packages/node/src/**/*.ts',
        'packages/validator/src/**/*.ts',
    ],

    // The directory where Jest should output its coverage files
    coverageDirectory: 'coverage',

    // Indicates which provider should be used to instrument code for coverage
    coverageProvider: 'v8',

    // A set of global variables that need to be available in all test environments
    // globals: {},
    globals: {
        'ts-jest': {
            tsconfig: 'tsconfig.build.json',
        },
    },

    // An array of file extensions your modules use

    moduleFileExtensions: ['js', 'json', 'ts'],

    // A map from regular expressions to module names or to arrays of module names that allow to stub out resources with a single module
    // moduleNameMapper: {},
    moduleNameMapper: {
        '^@subql/common': '<rootDir>/packages/common/src',
        '^@subql/common/(.*)$': '<rootDir>/packages/common/src/$1',
    },

    // The paths to modules that run some code to configure or set up the testing environment before each test
    // setupFiles: [],
    setupFiles: ['./jest-setup.ts'],

    // The test environment that will be used for testing
    testEnvironment: 'node',

    // The regexp pattern or array of patterns that Jest uses to detect test files
    testRegex: '.*\\.e2e-spec\\.ts$',

    // A map from regular expressions to paths to transformers
    transform: {
        '^.+\\.(ts|tsx)?$': 'ts-jest',
        '^.+\\.(js|jsx)$': 'babel-jest',
    },

    "transformIgnorePatterns": [
        "node_modules/(?!(@polkadot|@babel/runtime/helpers/esm)/)"
    ],

};