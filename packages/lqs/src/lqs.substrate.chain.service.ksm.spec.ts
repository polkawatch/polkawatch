// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';

// The API Connection may take time when connecting from far away
jest.setTimeout(10000);

import { ConfigModule } from '@nestjs/config';
import { SubstrateAPIService, SubstrateChainService, SubstrateMetadataService } from './lqs.substrate.chain.service';

import * as Joi from 'joi';

describe('Kusama SubstrateService', () => {
    let service: SubstrateChainService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    isGlobal: true,
                    validationSchema: Joi.object({
                        LQS_SUBSTRATE_RPC_URL: Joi.string().default('wss://kusama.valletech.eu'),
                    }),
                }),
            ],
            providers: [SubstrateAPIService, SubstrateMetadataService, SubstrateChainService],
        }).compile();

        service = module.get<SubstrateChainService>(SubstrateChainService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('will check some params', async () => {
        expect(await service.chainName()).toBe('Kusama');
        expect(await service.chainID()).toBe('2');
        expect(await service.tokenSymbol()).toBe('KSM');
        expect(await service.tokenDecimals()).toBe(12);
        expect(await service.daysToEras(1)).toBe(4);
        expect(await service.hasNominationPools()).toBeTruthy();
    });

});
