// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import { BaseQueryController } from './lqs.query.controller';
import { IndexQueryService, QueryTemplate } from './lqs.index.service';
import { RewardDistributionQuery } from './queries/query.parameters.dtos';
import { ArgumentMetadata, ValidationPipe } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { AppModule } from './lqs.module';
import { SubstrateAPIService, SubstrateChainService, SubstrateMetadataService } from './lqs.substrate.chain.service';

// The API Connection may take time when connecting from far away
jest.setTimeout(10000);

describe('BaseController Unit Tests - search and transformation with mocked elastic response', () => {

    let baseController: BaseQueryController;
    beforeAll(async () => {

        // Mock elastic service response
        const ElasticFake = {
            provide: ElasticsearchService,
            useFactory: () => ({
                search: jest.fn((x) => {
                    return x.body + 1;
                }),
            }),
        };

        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
            controllers: [BaseQueryController],
            providers: [ElasticFake, IndexQueryService, SubstrateAPIService, SubstrateMetadataService, SubstrateChainService ],
        }).compile();

        baseController = moduleFixture.get<BaseQueryController>(BaseQueryController);
    });

    it('checks if runQuery method on BaseController correctly returns response from ElasticService.search', async () => {
        const dto = 1;
        const dto2 = 3;
        const qt = (x) => x as QueryTemplate;
        const transformation = (x) => x;
        // @ts-ignore
        expect(await baseController.runQuery(dto, qt, transformation)).toEqual(2);
        // @ts-ignore
        expect(await baseController.runQuery(dto2, qt, transformation)).toEqual(4);
    });

    it('checks if transformation works as expected', async () => {
        const dto = 1;
        const dto2 = 3;
        const qt = (x) => x as QueryTemplate;
        const transformation = (x) => x * 10;
        // @ts-ignore
        expect(await baseController.runQuery(dto, qt, transformation)).toEqual(20);
        // @ts-ignore
        expect(await baseController.runQuery(dto2, qt, transformation)).toEqual(40);
    });
});

describe('Validation Unit Tests - test DTO against nestjs validation pipeline', () => {

    it('tests validation pipeline - using DistributionQueryDto', async () => {
        const testObject1 = {
            StartingEra: 500,
            TopResults: 10,
            RewardType: 'all',
            ValidatorIdentityType:'with identity',
            ValidatorType: 'public',
            NominatorType: 'all',
            PoolIdentityType: 'all',
        };
        const testObject2 = {
            StartingEra: '500',
            TopResults: 10,
        };
        const target: ValidationPipe = new ValidationPipe({ transform: true, whitelist: true });
        const metadata: ArgumentMetadata = {
            type: 'body',
            metatype: RewardDistributionQuery,
            data: '',
        };

        await expect(target.transform(testObject1, metadata)).resolves.toEqual(testObject1);

        await expect(target.transform(testObject2, metadata)).rejects.toThrowError();

    });
});