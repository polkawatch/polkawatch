// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiPromise, WsProvider } from '@polkadot/api';
import { BlockAuthoring, TrackingSystem } from './queries/query.responses.dtos';


/**
 * The Substrate Chain Service abstracts chain-specific information that is usually derived from
 * the Substrate API.
 *
 */
@Injectable()
export class SubstrateChainService {

    private readonly logger = new Logger(SubstrateChainService.name);

    constructor(@Inject('SUBSTRATE_METADATA') private chain, private configService: ConfigService) {
        // auto
    }

    /**
     * Returns the Chain Name for presentation purpose.
     */
    chainName() {
        return this.chain.chainName;
    }

    /**
     * Returns the Chain Identifier. This is used to compute addresses.
     */
    chainID() {
        return this.chain.chainID;
    }

    /**
     * Returns the token symbol for the blockchain for presentation purpose.
     */
    tokenSymbol() {
        return this.chain.tokenSymbol;
    }

    /**
     * Return the decimals used to turn reward event balances into actual token numbers.
     */
    tokenDecimals() {
        return this.chain.tokenDecimals;
    }

    /**
     * Returns the tokenDecimals used for reward calculation. Taking into account Aura parachains
     * Aura parachains rewards are tracked as authored blocks, in which case rewards have 0 decimals.
     */
    rewardTokenDecimals() {
        if(this.chain.blockAuthoring == 'Aura') return 0;
        else return this.tokenDecimals();
    }

    /**
     * Converts from days to eras/sessions, used for searches such as rewards in the last 30 days.
     */
    daysToEras(d) {
        if(this.chain.blockAuthoring == 'Babe') {
            const blocksPerEra = this.chain.sessionsPerEra * this.chain.epochDuration;
            const expectedBlockTimeS = this.chain.expectedBlockTime / 1000;
            const daysSeconds = d * 24 * 60 * 60;
            const daysBlocks = daysSeconds / expectedBlockTimeS;
            return daysBlocks / blocksPerEra;
        } else if (this.chain.blockAuthoring == 'Aura') {
            // it is hardcoded in code that a session is 6 hours, so there are 4 sessions in a day.
            return d * 4;
        }
    }

    /**
     * Returns information about staking pool feature.
     */

    hasNominationPools() {
        return this.chain.hasNominationPools;
    }

    /**
     * Returns whether this is a parachain or not
     */

    isParachain() {
        return this.chain.isParachain;
    }

    /**
     * Returns the relay chain for parachains and itself for relay chains
     */
    relayChainName() {
        return this.chain.relayChainName;
    }

    /**
     * Returns the reward measurement type
     */
    trackingSystem():TrackingSystem {
        switch(this.chain.blockAuthoring) {
        case 'Babe': return TrackingSystem.Tokens;
        case 'Aura': return TrackingSystem.Blocks;
        }
    }

    blockAuthoring(): BlockAuthoring {
        switch(this.chain.blockAuthoring) {
        case 'Babe': return BlockAuthoring.Babe;
        case 'Aura': return BlockAuthoring.Aura;
        }
    }

    hasStaking() {
        return this.chain.hasStaking;
    }
}

/**
 * Associated provider of the RPC api object.
 * Will return the connected api object after configuration.
 */
export const SubstrateAPIService = {
    provide: 'SUBSTRATE_API',
    useFactory: async (configService: ConfigService) => {
        const endpoint = configService.get('LQS_SUBSTRATE_RPC_URL');
        const logger = new Logger('SUBSTRATE_API');
        const wsProvider = new WsProvider(endpoint);
        const api = await ApiPromise.create({ provider: wsProvider });
        await api.isReady;
        logger.log(`Substrate API Ready with endpoint: ${endpoint}`);
        logger.log(`Chain is ${await api.rpc.system.chain()}`);
        logger.log(`Chain ID is ${api.consts.system.ss58Prefix}`);
        return api;
    },
    inject: [ConfigService],
};

export const SubstrateMetadataService = {
    provide: 'SUBSTRATE_METADATA',
    useFactory: async (api) => {
        const logger = new Logger('SUBSTRATE_METADATA');
        const chainName = (await api.rpc.system.chain()).toHuman();
        const chainID = api.consts.system.ss58Prefix.toHuman();
        const parachainID = api.query.parachainInfo ? (await api.query.parachainInfo.parachainId()).toHuman() : null;
        const isParachain = !!parachainID;
        const tokenSymbol = (await api.rpc.system.properties()).toHuman().tokenSymbol[0];
        const relayChainName = getRelayChainByTokenSymbol(tokenSymbol);
        const tokenDecimals = parseInt((await api.rpc.system.properties()).toHuman().tokenDecimals[0]);
        const blockAuthoring = getBlockAuthoring(api);
        const epochDuration = api.consts.babe?.epochDuration.toNumber();
        const hasStaking = !!api.consts.staking;
        const sessionsPerEra = api.consts.staking?.sessionsPerEra.toNumber();
        const expectedBlockTime = api.consts.babe?.expectedBlockTime.toNumber();
        const hasNominationPools = api.consts.staking ? parseInt((await api.query.nominationPools.palletVersion()).toHuman()) > 0 : false;
        logger.log(`${chainName} metadata stored`);

        return {
            chainName,
            chainID,
            isParachain,
            relayChainName,
            blockAuthoring,
            parachainID,
            tokenSymbol,
            tokenDecimals,
            hasStaking,
            epochDuration,
            sessionsPerEra,
            expectedBlockTime,
            hasNominationPools,
        };
    },
    inject: ['SUBSTRATE_API'],
};

/**
 * Returns the Relay Chain based on the Token Symbol
 * @param token
 */
function getRelayChainByTokenSymbol(token) {
    switch (token) {
    case 'KSM': return 'Kusama';
    case 'DOT': return 'Polkadot';
    default: throw new Error('Unknown Parachain');
    }

}

/**
 * Returns the block authoring mechanisms
 */
function getBlockAuthoring(api) {
    if(api.consts.babe) return 'Babe';
    else if(api.query.aura) return 'Aura';
    else return 'Unknown';
}