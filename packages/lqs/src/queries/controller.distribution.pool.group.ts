// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { BaseQueryController } from '../lqs.query.controller';
import { AggregatedIndexData, IndexQueryService, QueryTemplate } from '../lqs.index.service';
import { RewardsByPoolGroup } from './query.responses.dtos';
import { RewardDistributionQuery } from './query.parameters.dtos';
import { plainToInstance } from 'class-transformer';
import { SubstrateChainService } from '../lqs.substrate.chain.service';
import { QueryFilter } from './query.filter.util';


@ApiTags('pool')
@Controller()
export class PoolGroup extends BaseQueryController {
    constructor(protected queryService: IndexQueryService) {
        super(queryService);
    }

    @Post('distribution/pool/group')
    @ApiOperation({
        description: 'Get the distribution of Token Rewards per Pool Group',
    })
    @ApiOkResponse({ description: 'The distribution of Token Rewards per Pool Group', type: RewardsByPoolGroup, isArray: true })
    @HttpCode(HttpStatus.OK)
    async post(
        @Body() params: RewardDistributionQuery): Promise<Array<RewardsByPoolGroup>> {
        return (await super.runQuery(
            params,
            this.queryTemplate as QueryTemplate,
            this.queryResponseTransformer,
        )) as Array<RewardsByPoolGroup>;
    }

    queryResponseTransformer(indexResponse): Array<RewardsByPoolGroup> {
        const buckets = indexResponse.body.aggregations['polkawatch'].buckets as AggregatedIndexData;
        return plainToInstance(RewardsByPoolGroup, buckets, {
            excludeExtraneousValues: true,
        });
    }

    queryTemplate(params: RewardDistributionQuery, chain: SubstrateChainService) {
        return {
            'aggs': {
                polkawatch: {
                    'terms': {
                        'field': 'pool_parent',
                        'order': {
                            reward: 'desc',
                        },
                        'size': params.TopResults,
                    },
                    'aggs': {
                        name: {
                            'top_hits': {
                                'fields': [
                                    {
                                        'field': 'pool_parent_name',
                                    },
                                ],
                                '_source': false,
                                'size': 1,
                                'sort': [
                                    {
                                        'date': {
                                            'order': 'desc',
                                        },
                                    },
                                ],
                            },
                        },
                        reward: {
                            'sum': {
                                'script': {
                                    'source': `doc['reward'].value/${Math.pow(10, chain.rewardTokenDecimals())}.0`,
                                    'lang': 'painless',
                                },
                            },
                        },
                        regions: {
                            'cardinality': {
                                'field': 'validator_country_group_code',
                            },
                        },
                        countries: {
                            'cardinality': {
                                'field': 'validator_country_code',
                            },
                        },
                        networks: {
                            'cardinality': {
                                'field': 'validator_asn_code',
                            },
                        },
                        validator_groups: {
                            'cardinality': {
                                'field': 'validator_parent',
                            },
                        },
                        validators: {
                            'cardinality': {
                                'field': 'validator',
                            },
                        },
                        pools: {
                            'cardinality': {
                                'field': 'pool',
                            },
                        },
                    },
                },
            },
            'query': {
                'bool': {
                    'filter': QueryFilter(params),
                },
            },
        };
    }

}