// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BaseQueryController } from '../lqs.query.controller';
import { AggregatedIndexData, IndexQueryService, QueryTemplate } from '../lqs.index.service';
import { RewardEraEvolution, RegionalRewardEraEvolution } from './query.responses.dtos';
import { EvolutionQuery } from './query.parameters.dtos';
import { plainToInstance } from 'class-transformer';
import { SubstrateChainService } from '../lqs.substrate.chain.service';
import { QueryFilter } from './query.filter.util';

@ApiTags('geography')
@Controller()
export class GeoRegionEvolution extends BaseQueryController {
    constructor(protected queryService: IndexQueryService) {
        super(queryService);
    }

    @Post('evolution/geo/region')
    @ApiOperation({
        description: 'Get the evolution of Token Rewards/Authored Blocks per Region',
    })
    @ApiOkResponse({ description: 'The evolution of Token Rewards/Authored Blocks per Region', type: RegionalRewardEraEvolution, isArray: true })
    @HttpCode(HttpStatus.OK)
    async post(
        @Body() params: EvolutionQuery): Promise<Array<RegionalRewardEraEvolution>> {
        return (await super.runQuery(
            params,
            this.queryTemplate as QueryTemplate,
            this.queryResponseTransformer,
        )) as Array<RegionalRewardEraEvolution>;
    }

    queryResponseTransformer(indexResponse): Array<RegionalRewardEraEvolution> {
        const segments = indexResponse.body.aggregations['polkawatch'].buckets;

        return segments.map(segment => {
            const buckets = segment.eras.buckets as AggregatedIndexData;
            return {
                Id: segment.key,
                Segment: plainToInstance(RewardEraEvolution, buckets, {
                    excludeExtraneousValues: true,
                }),
            };
        });
    }

    queryTemplate(params: EvolutionQuery, chain: SubstrateChainService) {
        return {
            'aggs': {
                'polkawatch': {
                    'terms': {
                        'field': 'validator_country_group_code',
                        'order': {
                            'reward': 'desc',
                        },
                        'size': params.TopResults,
                    },
                    'aggs': {
                        'eras': {
                            'histogram': {
                                'field': 'era',
                                'interval': 1,
                                'min_doc_count': 1,
                            },
                            'aggs': {
                                'reward': {
                                    'sum': {
                                        'script': {
                                            'source': `doc['reward'].value/${Math.pow(10, chain.rewardTokenDecimals())}.0`,
                                            'lang': 'painless',
                                        },
                                    },
                                },
                            },
                        },
                        'reward': {
                            'sum': {
                                'script': {
                                    'source': `doc['reward'].value/${Math.pow(10, chain.rewardTokenDecimals())}.0`,
                                    'lang': 'painless',
                                },
                            },
                        },
                    },
                },
            },
            'query': {
                'bool': {
                    'filter': QueryFilter(params),
                },
            },
        };
    }
}

