// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags, ApiBody } from '@nestjs/swagger';
import { BaseQueryController } from '../lqs.query.controller';
import { AggregatedIndexData, IndexQueryService, QueryTemplate } from '../lqs.index.service';
import { InventoryRecord } from './query.responses.dtos';
import {
    InventoryType2ElasticField,
    InventoryQuery,
} from './query.parameters.dtos';
import { plainToInstance } from 'class-transformer';
import { SubstrateChainService } from '../lqs.substrate.chain.service';
import { QueryFilter } from './query.filter.util';

/**
 * Inventory Queries are meant to detect the set of available IDs.
 * This is meant main ly for IPFS dump generation by the DDP.
 *
 * The only big set is the nominator set, it can be reduced by Top N results by Reward size.
 */
@ApiTags('about')
@Controller()
export class DataSetInventory extends BaseQueryController {
    constructor(protected queryService: IndexQueryService) {
        super(queryService);
    }

    @Post('inventory/about/dataset')
    @ApiBody({ type: InventoryQuery })
    @ApiOperation({
        description: 'Get inventory information about the dataset',
    })
    @ApiOkResponse({ description: 'Returns inventory of requested records', type: InventoryRecord, isArray: true })
    @HttpCode(HttpStatus.OK)
    async post(
        @Body() params: InventoryQuery): Promise<Array<InventoryRecord>> {
        return (await super.runQuery(
            params,
            this.queryTemplate as QueryTemplate,
            this.queryResponseTransformer,
        )) as Array<InventoryRecord>;
    }

    queryResponseTransformer(indexResponse): Array<InventoryRecord> {
        const buckets = indexResponse.body.aggregations['polkawatch'].buckets as AggregatedIndexData;
        return plainToInstance(InventoryRecord, buckets, {
            excludeExtraneousValues: true,
        });
    }

    queryTemplate(params: InventoryQuery, chain: SubstrateChainService) {
        return {
            aggs: {
                polkawatch: {
                    terms: {
                        field: InventoryType2ElasticField[params.RecordType],
                        order: {
                            reward: 'desc',
                        },
                        size: params.TopResults,
                    },
                    aggs: {
                        reward: {
                            sum: {
                                script: {
                                    source: `doc['reward'].value/${Math.pow(10, chain.rewardTokenDecimals())}.0`,
                                    lang: 'painless',
                                },
                            },
                        },
                    },
                },
            },
            query: {
                bool: {
                    filter: QueryFilter(params),
                },
            },
        };
    }
}
