// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags, ApiBody } from '@nestjs/swagger';
import { BaseQueryController } from '../lqs.query.controller';
import { IndexQueryService, QueryTemplate } from '../lqs.index.service';
import { AboutDataQuality } from './query.responses.dtos';
import { AboutDataQualityQuery } from './query.parameters.dtos';
import { plainToInstance } from 'class-transformer';
import { SubstrateChainService } from '../lqs.substrate.chain.service';
import { QueryFilter } from './query.filter.util';
import assert from 'assert';

@ApiTags('about')
@Controller()
export class AboutDatasetQuality extends BaseQueryController {
    constructor(protected queryService: IndexQueryService) {
        super(queryService);
    }

    @Post('distribution/about/dataquality')
    @ApiBody({ type: AboutDataQualityQuery })
    @ApiOperation({
        description: 'Get information about the dataset',
    })
    @ApiOkResponse({ description: 'Returns information about the dataset', type: AboutDataQuality, isArray: false })
    @HttpCode(HttpStatus.OK)
    async post(
        @Body() params: AboutDataQualityQuery): Promise<AboutDataQuality> {
        return (await super.runQuery(
            params,
            this.queryTemplate as QueryTemplate,
            this.queryResponseTransformer,
        )) as AboutDataQuality;
    }

    queryResponseTransformer(indexResponse): AboutDataQuality {
        const aggregations = indexResponse.body.aggregations;

        try{
            // We assume, and verify, most reward events are traced
            aggregations.traced = aggregations.polkawatch.buckets[0];
            aggregations.not_traced = aggregations.polkawatch.buckets[1];
            assert(aggregations.traced?.key == 1, 'Fatal traceability error, ensure dataset has been indexed before querying, returning 0 data quality.');

            // We put the record count as an aggregation even it is not
            aggregations.traced.reward_events = indexResponse.body.hits.total;

            // We compute the data quality
            aggregations.traced.data_quality = 100 * aggregations.traced.reward.value / (aggregations.traced.reward.value + (aggregations.not_traced?.reward.value || 0));
        } catch (e) {
            // This is happenning because we are asking for quality of an empty set, or becuase the query is not
            // compatible with the blockchain indexed
            return {
                Eras: 0,
                RewardEvents: 0,
                DataQuality: 0,
                LastestEra: 0,
                LastUpdated: 0,
            } as AboutDataQuality;
        }


        return plainToInstance(AboutDataQuality, aggregations.traced, {
            excludeExtraneousValues: true,
        }) as AboutDataQuality;
    }

    queryTemplate(params: AboutDataQualityQuery, chain: SubstrateChainService) {

        // This query assumes that traced rewards are bigger than non-traced rewards
        // not-traced rewards should be a small percentage, but the order of buckets is expected like it
        return {
            'aggs': {
                polkawatch: {
                    'terms': {
                        'field': 'traced',
                        'order': {
                            reward: 'desc',
                        },
                        size: 2,
                    },
                    'aggs': {
                        'total_eras': {
                            'cardinality': {
                                'field': 'era',
                            },
                        },
                        'latest_era': {
                            'max': {
                                'field': 'era',
                            },
                        },
                        'last_updated': {
                            'max': {
                                'field': 'date',
                            },
                        },
                        'reward': {
                            'sum': {
                                'script': {
                                    'source': `doc['reward'].value/${Math.pow(10, chain.rewardTokenDecimals())}.0`,
                                    'lang': 'painless',
                                },
                            },
                        },
                    },
                },
            },
            'query': {
                'bool': {
                    // Query run over traced and untraced data
                    'filter': QueryFilter(params, 'all'),
                },
            },
        };
    }
}