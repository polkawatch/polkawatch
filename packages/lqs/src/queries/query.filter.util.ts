// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { FilteredBaseQuery, IdentityTypes } from './query.parameters.dtos';

// type for traceability queries
type Traceability = boolean | 'all';

/**
 * Query filter helper, will turn parameters into a filter expression. Note that by default
 * we consider only results that have been traced. For that reason, traceability needs to be
 * observed/monitored. Traceability metrics will be resported via queries, for that reason, a
 * parameter to override the default traceability query is provided.
 *
 * @param params query paramters to turn into query filter
 * @param traced traceability, defaults to true, use 'all' to omit the filter.
 * @constructor
 */
export function QueryFilter(params :FilteredBaseQuery, traced:Traceability = true) {
    traced = 'all';
    const filters = [
        {
            'range': {
                era: {
                    gte: params.StartingEra,
                },
            },
        },
        optionalFilter('traced', traced),
        optionalFilter('validator_identity', getValidatorIdentity(params)),
        optionalFilter('reward_type', params.RewardType),
        optionalFilter('validator_type', params.ValidatorType),
        optionalFilter('nominator_type', params.NominatorType),
        optionalFilter('validator_country_group_code', params.RegionFilter),
        optionalFilter('validator_country_code', params.CountryFilter),
        optionalFilter('validator_asn_code', params.NetworkFilter),
        optionalFilter('validator_parent', params.ValidatorGroupFilter),
        optionalFilter('validator', params.ValidatorFilter),
        optionalFilter('pool_identity', getPoolIdentity(params)),
        optionalFilter('pool_parent', params.PoolGroupFilter),
        optionalFilter('pool', params.PoolFilter),
        optionalFilter('nominator', params.NominatorFilter),
    ];

    // we will filter "undefined" filters, thouse not in use, before returning the filter clause for query

    return filters.filter((f)=>(f !== undefined));
}

/**
 * This function will construct a filter clause if the value is present and it is not "all"
 * otherwise undefined is returned instead.
 *
 * @param field
 * @param param
 */
function optionalFilter(field, param) {
    if(param != undefined) {
        if(param != 'all') {
            const match_phrase = {};
            match_phrase[field] = param;
            return { match: match_phrase };
        }
    }
}

/**
 * Identity type is managed differently in the Database, as boolean, and not enum
 * @param params the query parameters
 * @returns validator_identity field as boolean or undefined.
 */

function getValidatorIdentity(params) {

    let validator_identity:boolean = undefined;
    if(params.ValidatorIdentityType) {
        if(params.ValidatorIdentityType == IdentityTypes.WithIdentity) validator_identity = true;
        if(params.ValidatorIdentityType == IdentityTypes.Anonymous) validator_identity = false;
    }

    return validator_identity;
}

/**
 * Identity type is managed differently in the Database, as boolean, and not enum
 * @param params the query parameters
 * @returns pool_identity field as boolean or undefined.
 */

function getPoolIdentity(params) {

    let pool_identity:boolean = undefined;
    if(params.PoolIdentityType) {
        if(params.PoolIdentityType == IdentityTypes.WithIdentity) pool_identity = true;
        if(params.PoolIdentityType == IdentityTypes.Anonymous) pool_identity = false;
    }

    return pool_identity;
}