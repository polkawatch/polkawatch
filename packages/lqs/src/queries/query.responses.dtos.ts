// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Expose, Transform } from 'class-transformer';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';

/**
 * All queries will have as a response either a record or an array of records
 */
export type QueryResponse = QueryResponseRecord |
    Array<QueryResponseRecord> |
    Array<QueryResponseSegment<QueryResponseRecord>>;

/**
 * A segmented query returns multiple arrays of records
 */

export class QueryResponseSegment<T> {

    @ApiProperty({
        description: 'The segment ID',
    })
    @Expose({ name: 'key' })
        Id: string;

    // This field is not described for been to abstract, please override and provide a templated documentation.
    Segment: Array<T>;
}

/**
 * Here we list all possible Response Records we may get
 */
export type QueryResponseRecord = RewardsByRegion |
    RewardsByCountry |
    RewardsByNetworkProvider |
    RewardsByValidationGroup |
    RewardsByValidationNode |
    RewardsByPoolGroup |
    RewardsByPool |
    RewardsByNominatorType |
    AboutData |
    AboutDataQuality |
    EntitiesEraEvolution |
    RewardEraEvolution |
    InventoryRecord;

/**
 * Decentralizastion tracking methods: rewards for NPOS and blocks for Aura authorities
 */
export enum TrackingSystem {
    Tokens = 'tokens',
    Blocks = 'blocks'
}

/**
 * The block authoring method implemented by this blockchain
 */
export enum BlockAuthoring {
    Babe ='babe',
    Aura = 'aura'
}

/**
 * Chain metadata
 */

export class ChainMetadata {
    @ApiProperty({
        description: 'The Blockchain common name',
    })
        Name: string;

    @ApiProperty({
        description: 'The Blockchain SS58 Format number/identifier',
    })
        ID: number;

    @ApiProperty({
        description: 'The Blockchain token',
    })
        Token: string;

    @ApiProperty({
        description: 'Number of Eras/Sessions in 30 days',
    })
        ErasPerMonth: number;

    @ApiProperty({
        description: 'Whether the blockchain supports nomination pools',
    })
        HasNominationPools: boolean;

    @ApiProperty({
        description: 'Whether the blockchain is a Parachain',
    })
        IsParachain: boolean;

    @ApiProperty({
        description: 'The Relay chain of this Parachain',
    })
        RelayChain: string;

    @IsEnum(TrackingSystem)
    @ApiProperty({
        description: 'If decentralization is measured in Token Rewards or Authored Blocks',
        enum: ['tokens', 'blocks'],

    })
        TrackingSystem: string;

    @IsEnum(BlockAuthoring)
    @ApiProperty({
        description: 'The Block Authoring method used in this blockchain',
        enum: ['babe', 'aura'],

    })
        BlockAuthoring: string;

    @ApiProperty({
        description: 'Whether staking is supported in this Blockchain',
    })
        HasStaking: boolean;

}

/**
 * Inventory, just a collection of IDs for discovery use cases.
 */

export class InventoryRecord {
    @ApiProperty({
        description: 'The Record ID',
    })
    @Expose({ name: 'key' })
        Id: string;

    @ApiProperty({
        description: 'The Record Searchable Name',
    })
    @Expose({ name: 'name' })
        Name: string;
}

/**
 * Rewards Aggregations
 */

export class RewardAggregations {

    @ApiProperty({
        description: 'The sum of rewarded Tokens in reward events (NPoS) or authored blocks (Aura)',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'reward' })
        TokenRewards: number;

    @ApiProperty({
        description: 'The number of unique regions in reward events/authored blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'regions' })
        Regions: number;

    @ApiProperty({
        description: 'The number of unique countries in reward events/authored blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'countries' })
        Countries: number;

    @ApiProperty({
        description: 'The number of unique networks in reward events/authored blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'networks' })
        Networks: number;

    @ApiProperty({
        description: 'The number of unique validator/collator groups in reward events/authored blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'validator_groups' })
        ValidatorGroups: number;

    @ApiProperty({
        description: 'The number of unique validators/collators in reward events/authored blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'validators' })
        Validators: number;

    @ApiProperty({
        description: 'The number of unique pool groups in reward events',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'pool_groups' })
        PoolGroups: number;

    @ApiProperty({
        description: 'The number of unique pools in reward events',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'pools' })
        Pools: number;

    @ApiProperty({
        description: 'The number of unique nominators in reward events',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'nominators' })
        Nominators: number;

}

/**
 * About Data, this type mixes both data quality and data summary information
 */

export class AboutData extends RewardAggregations {
    @ApiProperty({
        description: 'The count of eras/sessions for which rewards/blocks are present',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'total_eras' })
        Eras: number;

    @ApiProperty({
        description: 'The count of reward events/authored blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'reward_events' })
        RewardEvents: number;

    @ApiProperty({
        description: 'The latest era/session for which rewards/authored blocks are present',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'latest_era' })
        LastestEra: number;

    @ApiProperty({
        description: 'Date of the last update of the dataset, milliseconds since 1 January 1970 UTC',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'last_updated' })
        LastUpdated: number;
}

/**
 * About Data Quality.
 */

export class AboutDataQuality {
    @ApiProperty({
        description: 'The count of eras/sessions for which rewards/authored blocks are present',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'total_eras' })
        Eras: number;

    @ApiProperty({
        description: 'The count of reward events/authored blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'reward_events' })
        RewardEvents: number;

    @ApiProperty({
        description: 'The latest era/session for which rewards/blocks are present',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'latest_era' })
        LastestEra: number;

    @ApiProperty({
        description: 'Date of the last update of the dataset, milliseconds since 1 January 1970 UTC',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'last_updated' })
        LastUpdated: number;

    @ApiProperty({
        description: 'Percentage of rewards/blocks that could be completely traced',
    })
    @Transform(({ value }) => value, { toClassOnly: true })
    @Expose({ name: 'data_quality' })
        DataQuality: number;

}

/**
 * Rewards by Region
 */

export class RewardsByRegion extends PickType(RewardAggregations, ['TokenRewards', 'Countries', 'Networks', 'ValidatorGroups', 'Validators', 'PoolGroups', 'Pools', 'Nominators'] as const) {

    @ApiProperty({
        description: 'The region ID',
    })
    @Expose({ name: 'key' })
        Id: string;

    @ApiProperty({
        description: 'The region name',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.validator_country_group_name['0'], { toClassOnly: true })
  @Expose({ name: 'name' })
        Region: string;
}

/**
 * Rewards by Country
 */

export class RewardsByCountry extends PickType(RewardAggregations, ['TokenRewards', 'Networks', 'ValidatorGroups', 'Validators', 'PoolGroups', 'Pools', 'Nominators'] as const) {
    @ApiProperty({
        description: 'The country ID, its ISO Code',
    })
    @Expose({ name: 'key' })
        Id: string;

    @ApiProperty({
        description: 'The country name',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.validator_country_name['0'], { toClassOnly: true })
    @Expose({ name: 'name' })
        Country: string;

}

/**
 * Rewards by Computing Network Provider
 */

export class RewardsByNetworkProvider extends PickType(RewardAggregations, ['TokenRewards', 'Regions', 'Countries', 'ValidatorGroups', 'Validators', 'PoolGroups', 'Pools', 'Nominators'] as const) {

    @ApiProperty({
        description: 'The network provider ID, its ASN number',
    })
    @Expose({ name: 'key' })
        Id: string;

    @ApiProperty({
        description: 'The network provider name',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.validator_asn_name['0'], { toClassOnly: true })
    @Expose({ name: 'name' })
        NetworkProvider: string;

}

/**
 * Rewards by Validator Group
 */

export class RewardsByValidationGroup extends PickType(RewardAggregations, ['TokenRewards', 'Regions', 'Countries', 'Networks', 'Validators', 'PoolGroups', 'Pools', 'Nominators'] as const) {

    @ApiProperty({
        description: 'The validator/collator id for the group',
    })
    @Expose({ name: 'key' })
        Id: string;

    @ApiProperty({
        description: 'The validator/collator group name',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.validator_parent_name['0'], { toClassOnly: true })
    @Expose({ name: 'name' })
        ValidationGroup: string;

    @ApiProperty({
        description: 'The median nomination in Tokens for this validator group',
    })
    @Transform(({ value }) => value.values['50.0'], { toClassOnly: true })
    @Expose({ name: 'median_nomination' })
        TokenMedianNomination: number;

}

export class RewardsByValidationNode extends PickType(RewardAggregations, ['TokenRewards', 'Regions', 'Countries', 'Networks', 'PoolGroups', 'Pools', 'Nominators'] as const) {

    @ApiProperty({
        description: 'The id for the validator/collator',
    })
    @Expose({ name: 'key' })
        Id: string;

    @ApiProperty({
        description: 'The validator/collator name',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.validator_name['0'], { toClassOnly: true })
    @Expose({ name: 'name' })
        Validator: string;

    @ApiProperty({
        description: 'The last region recorded',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.validator_country_group_name['0'], { toClassOnly: true })
    @Expose({ name: 'last_region' })
        LastRegion: string;

    @ApiProperty({
        description: 'The last country recorded',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.validator_country_name['0'], { toClassOnly: true })
    @Expose({ name: 'last_country' })
        LastCountry: string;

    @ApiProperty({
        description: 'The last network recorded',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.validator_asn_name['0'], { toClassOnly: true })
    @Expose({ name: 'last_network' })
        LastNetwork: string;

    @ApiProperty({
        description: 'The Operator ID this Node belongs too',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.validator_parent['0'], { toClassOnly: true })
    @Expose({ name: 'validator_parent' })
        operatorParent: string;

    @ApiProperty({
        description: 'The Operator Name this Node belongs too',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.validator_parent_name['0'], { toClassOnly: true })
    @Expose({ name: 'validator_parent_name' })
        operatorParentName: string;

    @ApiProperty({
        description: 'The median nomination in Tokens for this validator',
    })
    @Transform(({ value }) => value.values['50.0'], { toClassOnly: true })
    @Expose({ name: 'median_nomination' })
        TokenMedianNomination: number;
}

/**
 * Rewards by Pool Group
 */

export class RewardsByPoolGroup extends PickType(RewardAggregations, ['TokenRewards', 'Regions', 'Countries', 'Networks', 'ValidatorGroups', 'Validators', 'Pools'] as const) {

    @ApiProperty({
        description: 'The validator id for the pool group',
    })
    @Expose({ name: 'key' })
        Id: string;

    @ApiProperty({
        description: 'The pool group name',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.pool_parent_name['0'], { toClassOnly: true })
    @Expose({ name: 'name' })
        PoolGroup: string;

}

export class RewardsByPool extends PickType(RewardAggregations, ['TokenRewards', 'Regions', 'Countries', 'Networks', 'ValidatorGroups', 'Validators' ] as const) {

    @ApiProperty({
        description: 'The id for the pool',
    })
    @Expose({ name: 'key' })
        Id: string;

    @ApiProperty({
        description: 'The pool name',
    })
    @Transform(({ value }) => value.hits.hits['0'].fields.pool_name['0'], { toClassOnly: true })
    @Expose({ name: 'name' })
        Pool: string;

}


export class RewardsByNominatorType extends PickType(RewardAggregations, ['TokenRewards', 'Regions', 'Countries', 'Networks', 'ValidatorGroups', 'Validators' ] as const) {

    @ApiProperty({
        description: 'The nominator type',
    })
    @Expose({ name: 'key' })
        NominatorType: string;

}


/**
 * Evolution, by Era Responses
 */

export class EraEvolution {
    @ApiProperty({
        description: 'The staking era/session number',
    })
    @Expose({ name: 'key' })
        Era: number;
}

export class EntitiesEraEvolution extends EraEvolution {
    @ApiProperty({
        description: 'The unique count countries of validators/collators that produced rewards/blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'countries' })
        Countries: number;

    @ApiProperty({
        description: 'The unique count of computing networks of the validators/collators that produced rewards/blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'networks' })
        Networks: number;

    @ApiProperty({
        description: 'The unique count of validator/collator groups that produced rewards/blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'validator_groups' })
        ValidatorGroups: number;

    @ApiProperty({
        description: 'The unique count of validators/collators that produced rewards/blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'validators' })
        Validators: number;

    @ApiProperty({
        description: 'The unique count of nominators that produced rewards',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'nominators' })
        Nominators: number;
}


export class RewardEraEvolution extends EraEvolution {
    @ApiProperty({
        description: 'The rewards in Tokens/Authored Blocks',
    })
    @Transform(({ value }) => value.value, { toClassOnly: true })
    @Expose({ name: 'reward' })
        TokenRewards: number;
}

export class RegionalRewardEraEvolution extends QueryResponseSegment<RewardEraEvolution> {

    @ApiProperty({
        description: 'The segment of Reward/Blocks per Era/Session ',
        type: RewardEraEvolution,
        isArray: true,
    })
        Segment: Array<RewardEraEvolution>;
}