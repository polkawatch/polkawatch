// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BaseQueryController } from '../lqs.query.controller';
import { AggregatedIndexData, IndexQueryService, QueryTemplate } from '../lqs.index.service';
import { RewardsByValidationNode } from './query.responses.dtos';
import { RewardDistributionQuery } from './query.parameters.dtos';
import { plainToInstance } from 'class-transformer';
import { SubstrateChainService } from '../lqs.substrate.chain.service';
import { QueryFilter } from './query.filter.util';

@ApiTags('validator')
@Controller()
export class ValidatorNode extends BaseQueryController {
    constructor(protected queryService: IndexQueryService) {
        super(queryService);
    }

    @Post('distribution/validator/node')
    @ApiOperation({
        description: 'Get the distribution of Token Rewards/Authored Blocks per Validator/Collator Node',
    })
    @ApiOkResponse({ description: 'The distribution of Token Rewards/Authored Blocks per Validator/Collator Node', type: RewardsByValidationNode, isArray: true })
    @HttpCode(HttpStatus.OK)
    async post(
        @Body() params: RewardDistributionQuery): Promise<Array<RewardsByValidationNode>> {
        return (await super.runQuery(
            params,
            this.queryTemplate as QueryTemplate,
            this.queryResponseTransformer,
        )) as Array<RewardsByValidationNode>;
    }

    queryResponseTransformer(indexResponse): Array<RewardsByValidationNode> {
        const buckets = indexResponse.body.aggregations['polkawatch'].buckets as AggregatedIndexData;
        return plainToInstance(RewardsByValidationNode, buckets, {
            excludeExtraneousValues: true,
        });
    }

    queryTemplate(params: RewardDistributionQuery, chain: SubstrateChainService) {
        return {
            'aggs': {
                polkawatch: {
                    'terms': {
                        'field': 'validator',
                        'order': {
                            reward: 'desc',
                        },
                        'size': params.TopResults,
                    },
                    'aggs': {
                        name: {
                            'top_hits': {
                                'fields': [
                                    {
                                        'field': 'validator_name',
                                    },
                                ],
                                '_source': false,
                                'size': 1,
                                'sort': [
                                    {
                                        'date': {
                                            'order': 'desc',
                                        },
                                    },
                                ],
                            },
                        },
                        last_region: {
                            'top_hits': {
                                'fields': [
                                    {
                                        'field': 'validator_country_group_name',
                                    },
                                ],
                                '_source': false,
                                'size': 1,
                                'sort': [
                                    {
                                        'date': {
                                            'order': 'desc',
                                        },
                                    },
                                ],
                            },
                        },
                        last_country: {
                            'top_hits': {
                                'fields': [
                                    {
                                        'field': 'validator_country_name',
                                    },
                                ],
                                '_source': false,
                                'size': 1,
                                'sort': [
                                    {
                                        'date': {
                                            'order': 'desc',
                                        },
                                    },
                                ],
                            },
                        },
                        last_network: {
                            'top_hits': {
                                'fields': [
                                    {
                                        'field': 'validator_asn_name',
                                    },
                                ],
                                '_source': false,
                                'size': 1,
                                'sort': [
                                    {
                                        'date': {
                                            'order': 'desc',
                                        },
                                    },
                                ],
                            },
                        },
                        validator_parent: {
                            'top_hits': {
                                'fields': [
                                    {
                                        'field': 'validator_parent',
                                    },
                                ],
                                '_source': false,
                                'size': 1,
                                'sort': [
                                    {
                                        'date': {
                                            'order': 'desc',
                                        },
                                    },
                                ],
                            },
                        },
                        validator_parent_name: {
                            'top_hits': {
                                'fields': [
                                    {
                                        'field': 'validator_parent_name',
                                    },
                                ],
                                '_source': false,
                                'size': 1,
                                'sort': [
                                    {
                                        'date': {
                                            'order': 'desc',
                                        },
                                    },
                                ],
                            },
                        },
                        reward: {
                            'sum': {
                                'script': {
                                    'source': `doc['reward'].value/${Math.pow(10, chain.rewardTokenDecimals())}.0`,
                                    'lang': 'painless',
                                },
                            },
                        },
                        regions: {
                            'cardinality': {
                                'field': 'validator_country_group_code',
                            },
                        },
                        countries: {
                            'cardinality': {
                                'field': 'validator_country_code',
                            },
                        },
                        networks: {
                            'cardinality': {
                                'field': 'validator_asn_code',
                            },
                        },
                        validators: {
                            'cardinality': {
                                'field': 'validator',
                            },
                        },
                        pool_groups: {
                            'cardinality': {
                                'field': 'pool_parent',
                            },
                        },
                        pools: {
                            'cardinality': {
                                'field': 'pool',
                            },
                        },
                        nominators: {
                            'cardinality': {
                                'field': 'nominator',
                            },
                        },
                        median_nomination: {
                            'percentiles': {
                                'script': {
                                    'source': `if (doc['nomination_value'].size()!=0) return doc['nomination_value'].value/${Math.pow(10, chain.rewardTokenDecimals())}.0;`,
                                    'lang': 'painless',
                                },
                                'percents': [
                                    50,
                                ],
                            },
                        },
                    },
                },
            },
            'query': {
                'bool': {
                    'filter': QueryFilter(params),
                },
            },
        };
    }
}

