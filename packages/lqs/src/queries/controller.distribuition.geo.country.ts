// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BaseQueryController } from '../lqs.query.controller';
import { AggregatedIndexData, IndexQueryService, QueryTemplate } from '../lqs.index.service';
import { RewardsByCountry } from './query.responses.dtos';
import { RewardDistributionQuery } from './query.parameters.dtos';
import { plainToInstance } from 'class-transformer';
import { SubstrateChainService } from '../lqs.substrate.chain.service';
import { QueryFilter } from './query.filter.util';

@ApiTags('geography')
@Controller()
export class GeoCountry extends BaseQueryController {
    constructor(protected queryService: IndexQueryService) {
        super(queryService);
    }

    @Post('distribution/geo/country')
    @ApiOperation({
        description: 'Get the distribution of Token Rewards/Authored Blocks per Country',
    })
    @ApiOkResponse({ description: 'The distribution of Token Rewards/Authored Blocks per Country', type: RewardsByCountry, isArray: true })
    @HttpCode(HttpStatus.OK)
    async post(
        @Body() params: RewardDistributionQuery): Promise<Array<RewardsByCountry>> {
        return (await super.runQuery(
            params,
            this.queryTemplate as QueryTemplate,
            this.queryResponseTransformer,
        )) as Array<RewardsByCountry>;
    }

    queryResponseTransformer(indexResponse): Array<RewardsByCountry> {
        const buckets = indexResponse.body.aggregations['polkawatch'].buckets as AggregatedIndexData;
        return plainToInstance(RewardsByCountry, buckets, {
            excludeExtraneousValues: true,
        });
    }

    queryTemplate(params: RewardDistributionQuery, chain: SubstrateChainService) {
        return {
            'aggs': {
                polkawatch: {
                    'terms': {
                        'field': 'validator_country_code',
                        'order': {
                            reward: 'desc',
                        },
                        size: params.TopResults,
                    },
                    'aggs': {
                        name: {
                            'top_hits': {
                                'fields': [
                                    {
                                        'field': 'validator_country_name',
                                    },
                                ],
                                '_source': false,
                                'size': 1,
                                'sort': [
                                    {
                                        'date': {
                                            'order': 'desc',
                                        },
                                    },
                                ],
                            },
                        },
                        reward: {
                            'sum': {
                                'script': {
                                    'source': `doc['reward'].value/${Math.pow(10, chain.rewardTokenDecimals())}.0`,
                                    'lang': 'painless',
                                },
                            },
                        },
                        networks: {
                            'cardinality': {
                                'field': 'validator_asn_code',
                            },
                        },
                        validator_groups: {
                            'cardinality': {
                                'field': 'validator_parent',
                            },
                        },
                        validators: {
                            'cardinality': {
                                'field': 'validator',
                            },
                        },
                        pool_groups: {
                            'cardinality': {
                                'field': 'pool_parent',
                            },
                        },
                        pools: {
                            'cardinality': {
                                'field': 'pool',
                            },
                        },
                        nominators: {
                            'cardinality': {
                                'field': 'nominator',
                            },
                        },
                    },
                },
            },
            'query': {
                'bool': {
                    'filter': QueryFilter(params),
                },
            },
        };
    }
}
