// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Controller, Logger } from '@nestjs/common';
import { QueryParameters } from './queries/query.parameters.dtos';
import {
    IndexQueryService,
    QueryResponseTransformer,
    QueryTemplate,
} from './lqs.index.service';


/**
 * The Base Query Controller is responsible for implementing the API Request flow with the following steps:
 *
 * - Validation
 * - Query
 * - Transformation
 *
 * Most LQS Queries follow this pattern, but provide custom parameters, template and transformer. The exception are
 * queries about Chain metadata.
 *
 */

@Controller()
export class BaseQueryController {
    readonly logger = new Logger(BaseQueryController.name);

    constructor(protected queryService: IndexQueryService) {
        // do nothing.
    }

    runQuery(
        parameters: QueryParameters,
        queryTemplate: QueryTemplate,
        queryResponseTransformer: QueryResponseTransformer,
    ) {
        this.logger.debug('Input is valid, running query');
        return this.queryService.runQuery(
            parameters,
            queryTemplate,
            queryResponseTransformer,
        );
    }
}
