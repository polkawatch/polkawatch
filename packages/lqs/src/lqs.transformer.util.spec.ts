// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0


import { plainToInstance } from 'class-transformer';
import { RewardsByRegion } from './queries/query.responses.dtos';

/**
 * Class transform unit testing. Due to its annotations this utility seems difficult to make work after
 * updating the dependency tree. This is basically a regression unit testing.
 */
describe('Transformation Utilities', () => {

    it('Will test Rewards by Region transformer annotations', () => {
        const rewards = plainToInstance(RewardsByRegion, TEST_BUCKETS_DATA, {
            excludeExtraneousValues: true,
        });
        expect(rewards.length).toBe(5);

        rewards.forEach((r)=>{
            ['TokenRewards', 'Countries', 'ValidatorGroups', 'Validators', 'Nominators'].forEach((f)=>{
                expect(r[f]).toBeGreaterThan(0);
            });
        });

    });
});

/**
 * This test data is the buckets variable in the geo region query.
 */
const TEST_BUCKETS_DATA = [
    {
        'key': 'EU',
        'doc_count': 617214,
        'validator_groups': {
            'value': 294,
        },
        'reward': {
            'value': 16695.61454999409,
        },
        'validators': {
            'value': 650,
        },
        'nominators': {
            'value': 7368,
        },
        'pool_groups': {
            'value': 46,
        },
        'name': {
            'hits': {
                'total': {
                    'value': 617214,
                    'relation': 'eq',
                },
                'max_score': null,
                'hits': [
                    {
                        '_index': 'pw_reward',
                        '_type': '_doc',
                        '_id': '14893097-47',
                        '_score': null,
                        'fields': {
                            'validator_country_group_name': [
                                'Europe',
                            ],
                        },
                        'sort': [
                            1665839124012,
                        ],
                    },
                ],
            },
        },
        'pools': {
            'value': 53,
        },
        'countries': {
            'value': 24,
        },
        'networks': {
            'value': 49,
        },
    },
    {
        'key': 'NA',
        'doc_count': 88832,
        'validator_groups': {
            'value': 132,
        },
        'reward': {
            'value': 2283.99384503504,
        },
        'validators': {
            'value': 170,
        },
        'nominators': {
            'value': 3969,
        },
        'pool_groups': {
            'value': 23,
        },
        'name': {
            'hits': {
                'total': {
                    'value': 88832,
                    'relation': 'eq',
                },
                'max_score': null,
                'hits': [
                    {
                        '_index': 'pw_reward',
                        '_type': '_doc',
                        '_id': '14893087-66',
                        '_score': null,
                        'fields': {
                            'validator_country_group_name': [
                                'North America',
                            ],
                        },
                        'sort': [
                            1665839064006,
                        ],
                    },
                ],
            },
        },
        'pools': {
            'value': 26,
        },
        'countries': {
            'value': 3,
        },
        'networks': {
            'value': 30,
        },
    },
    {
        'key': 'AS',
        'doc_count': 16140,
        'validator_groups': {
            'value': 15,
        },
        'reward': {
            'value': 459.059028800091,
        },
        'validators': {
            'value': 31,
        },
        'nominators': {
            'value': 1644,
        },
        'pool_groups': {
            'value': 7,
        },
        'name': {
            'hits': {
                'total': {
                    'value': 16140,
                    'relation': 'eq',
                },
                'max_score': null,
                'hits': [
                    {
                        '_index': 'pw_reward',
                        '_type': '_doc',
                        '_id': '14893200-100',
                        '_score': null,
                        'fields': {
                            'validator_country_group_name': [
                                'Asia',
                            ],
                        },
                        'sort': [
                            1665839742013,
                        ],
                    },
                ],
            },
        },
        'pools': {
            'value': 9,
        },
        'countries': {
            'value': 7,
        },
        'networks': {
            'value': 9,
        },
    },
    {
        'key': 'OC',
        'doc_count': 107,
        'validator_groups': {
            'value': 1,
        },
        'reward': {
            'value': 19.09351785342,
        },
        'validators': {
            'value': 2,
        },
        'nominators': {
            'value': 16,
        },
        'pool_groups': {
            'value': 0,
        },
        'name': {
            'hits': {
                'total': {
                    'value': 107,
                    'relation': 'eq',
                },
                'max_score': null,
                'hits': [
                    {
                        '_index': 'pw_reward',
                        '_type': '_doc',
                        '_id': '14870582-133',
                        '_score': null,
                        'fields': {
                            'validator_country_group_name': [
                                'Oceania',
                            ],
                        },
                        'sort': [
                            1665703968020,
                        ],
                    },
                ],
            },
        },
        'pools': {
            'value': 0,
        },
        'countries': {
            'value': 1,
        },
        'networks': {
            'value': 1,
        },
    },
    {
        'key': 'SA',
        'doc_count': 148,
        'validator_groups': {
            'value': 1,
        },
        'reward': {
            'value': 7.251679351158,
        },
        'validators': {
            'value': 1,
        },
        'nominators': {
            'value': 40,
        },
        'pool_groups': {
            'value': 0,
        },
        'name': {
            'hits': {
                'total': {
                    'value': 148,
                    'relation': 'eq',
                },
                'max_score': null,
                'hits': [
                    {
                        '_index': 'pw_reward',
                        '_type': '_doc',
                        '_id': '14892165-66',
                        '_score': null,
                        'fields': {
                            'validator_country_group_name': [
                                'South America',
                            ],
                        },
                        'sort': [
                            1665833532012,
                        ],
                    },
                ],
            },
        },
        'pools': {
            'value': 0,
        },
        'countries': {
            'value': 1,
        },
        'networks': {
            'value': 1,
        },
    },
];