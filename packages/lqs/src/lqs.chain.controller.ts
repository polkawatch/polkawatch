// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Controller, Get, Logger } from '@nestjs/common';
import { SubstrateChainService } from './lqs.substrate.chain.service';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ChainMetadata } from './queries/query.responses.dtos';

/**
 * The Chain Controller provides metadata about the blockchain. This is mainly to allow for multichain deployments
 * and the DAPP UI to be able to present chain-dependant information.
 */

@ApiTags('about')
@Controller()
export class Chain {

    readonly logger = new Logger(Chain.name);

    constructor(protected chain: SubstrateChainService) {
        // auto
    }

    @Get('/chain/meta')
    @ApiOkResponse({ description: 'Returns metadata about the blockchain', type: ChainMetadata })
    meta() {
        return {
            Name: this.chain.chainName(),
            Token: this.chain.tokenSymbol(),
            ID: this.chain.chainID(),
            ErasPerMonth: this.chain.daysToEras(30),
            HasNominationPools: this.chain.hasNominationPools(),
            IsParachain: this.chain.isParachain(),
            RelayChain: this.chain.relayChainName(),
            TrackingSystem: this.chain.trackingSystem(),
            BlockAuthoring: this.chain.blockAuthoring(),
            HasStaking: this.chain.hasStaking(),
        };
    }


}