// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { ConfigService } from '@nestjs/config';
import { Inject, Injectable, Logger } from '@nestjs/common';

import {
    Configuration,
    MetaApi,
    AuthorityApi, GeoIpLocation,
} from '@polkawatch/toporacle-client';
import LRU from 'lru-cache';

@Injectable()
export class ToporacleService {

    private readonly logger = new Logger(ToporacleService.name);
    private readonly locationCache;

    constructor(@Inject('TOPORACLE_API') private api: ToporacleApi, private readonly configService:ConfigService) {
        // We want a very small cache, the purpose is not to hit constantly with all rewards of a validator...
        // but we want to check constantly for location in case there are updates.
        const validators = configService.get('INDEXER_CACHE_VALIDATORS') / 100;
        this.locationCache = new LRU({ max:validators });
    }

    /**
     * We will process every reward event adding Geolocation and Networking information (ASN)
     * @param reward
     */
    async processReward(reward):Promise<any> {
        reward.traced = false;

        // The authority info that was discovered
        const geo_ip_location: GeoIpLocation = await this.cachedIPLocation(reward);

        if (geo_ip_location) reward.traced = true;

        // Ensure we set the default UI expected values
        reward.geo_country_display = {
            group_code: geo_ip_location?.country?.group_code || 'NOT_TRACED',
            group_name: geo_ip_location?.country?.group_name || 'NOT_TRACED',
            country_code: geo_ip_location?.country?.country_code || 'NOT_TRACED',
            country_name: geo_ip_location?.country?.country_name || 'NOT_TRACED',
        };
        reward.geo_asn_display = {
            asn_group_code: 'NOT_USED',
            asn_group_name: geo_ip_location?.network?.asn_group_name || 'NOT_TRACED',
            asn_code: geo_ip_location?.network?.asn_code || 'NOT_TRACED',
            asn_name: geo_ip_location?.network?.asn_name || 'NOT_TRACED',
        };

        return reward;
    }

    async cachedIPLocation(reward):Promise<any> {
        const cacheKey = `ipl-${reward.validator.id}`;

        if (this.locationCache.has(cacheKey)) {return this.locationCache.get(cacheKey);} else {
            const ipLocation = this.getIPLocation(reward);
            this.locationCache.set(cacheKey, ipLocation);
            return ipLocation;
        }
    }

    async getIPLocation(reward):Promise<any> {

        // The validator that paid the reward out
        const validator = reward.validator.id;

        let geo_ip_location:GeoIpLocation;

        try {
            const authority_info = await this.api.authority.getOwner({
                id: validator,
            });

            const location = authority_info.data.bestLocation;

            switch (location.discovery_type) {
            case 'Identified':
                geo_ip_location = location.geo_location;
                this.logger.debug(`Toporacle location for validator ${validator} identified`);
                break;
            case 'Declared':
                // Check if we have more than 1 location.
                // TopOracle is responsible for reducing them, if they are the same
                // We take the first and Warn if multiple IPs resulted in multiple locations
                if(location.geo_locations.length > 1) this.logger.warn(`Mutiple Declared locations for validator ${validator}, picking first`);
                geo_ip_location = location.geo_locations[0];
                break;
            case 'Unknown':
                // location not traced we only warn
                this.logger.warn(`Location for validator ${validator}, unknown`);
            }
        } catch (e) {
            this.logger.error(`Toporacle location for validator ${validator} failed.`);
        }
        return geo_ip_location;
    }
}

export type ToporacleApi = {
    meta: MetaApi;
    authority: AuthorityApi;
}

function axiosProxyOptions(proxy) {
    if(proxy) {
        const url = new URL(proxy);
        return {
            proxy:{
                protocol: url.protocol,
                host: url.hostname,
                port: url.port,
            },
        };
    }

}

/**
 * Associated provider of the RPC api object.
 * Will return the connected api object after configuration.
 */
export const ToporacleAPIService = {
    provide: 'TOPORACLE_API',
    useFactory: async (configService: ConfigService) => {
        const endpoint = configService.get('INDEXER_TOPORACLE_URL');
        const proxy = configService.get('INDEXER_TOPORACLE_PROXY');


        const apiConfig = new Configuration({
            basePath: endpoint,
            baseOptions: axiosProxyOptions(proxy),
        });

        const api: ToporacleApi = {
            meta: new MetaApi(apiConfig),
            authority: new AuthorityApi(apiConfig),
        };

        const logger = new Logger('TOPORACLE_API');
        logger.log(`Connected to TopOracle ${await api.meta.getVersion()}`);
        logger.log(`Toporacle Chain is ${(await api.meta.getChainInfo()).data.name}`);
        return api;
    },
    inject: [ConfigService],
};