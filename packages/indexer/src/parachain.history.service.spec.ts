// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { ApiPromise } from '@polkadot/api';
import { ParachainHistoryService, ParahainAPIService } from './parachain.history.service';
import * as process from 'process';
import { RelayChainAPIService, RelaychainHistoryService } from './relaychain.history.service';
import { IdentityAPIService, IdentityService } from './identity.service';

jest.setTimeout(60 * 1000);

describe('Parachain History Service', () => {
    let api:ApiPromise;
    let service: ParachainHistoryService;


    beforeEach(async () => {
        process.env.INDEXER_MODE = 'PARACHAIN';
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    isGlobal: true,
                    validationSchema: Joi.object({
                        INDEXER_RELAYCHAIN_RPC_URL: Joi.string().default('wss://kusama.valletech.eu'),
                        INDEXER_PARACHAIN_RPC_URL: Joi.string().default('wss://statemine.valletech.eu'),
                        // INDEXER_PARACHAIN_RPC_URL: Joi.string().default('wss://statemine-rpc.polkadot.io'),
                        INDEXER_IDENTITY_RPC_URL: Joi.string().default('wss://kusama-people-rpc.polkadot.io'),
                        INDEXER_ARCHIVE_HOST: Joi.string().default('localhost'),
                        INDEXER_ARCHIVE_PORT: Joi.number().default(3000),
                        INDEXER_CACHE_VALIDATORS: Joi.number().default(300),
                        INDEXER_CACHE_OPEN_ERAS: Joi.number().default(5),
                    }),
                }),
            ],
            providers: [ParahainAPIService, ParachainHistoryService, RelaychainHistoryService, RelayChainAPIService, IdentityAPIService, IdentityService],
        }).compile();

        api = module.get('PARACHAIN_API');
        service = module.get(ParachainHistoryService);
    });

    it('should be defined', () => {
        expect(api).toBeDefined();
        expect(service).toBeDefined();
    });

    it('Get Block Author Information', async () =>{
        // const blockNumber=17877287;
        // const blockNumber=4468229;
        const blockNumber = 1468229;
        let block = {
            id: blockNumber,
            validatorId: undefined,
        };
        block = await service.addBlockAuthorship(block);
        expect(block.validatorId).toBeDefined();
    });

    it('Will get collator info for one block', async () => {
        const blockNumber = 4494770;
        let block = {
            id: blockNumber,
            validator: undefined,
        };
        block = await service.addBlockAuthorship(block);
        block = await service.addCollatorInfo(block);
        expect(block.validator.info).toBeDefined();
    });

    it('Will get collator last HB for one block', async () => {
        const blockNumber = 4494771;
        let block = {
            id: blockNumber,
            session: undefined,
            timeStamp: undefined,
        };
        block = await service.addBlockAuthorship(block);
        block = await service.addBlockTiming(block);
        expect(block.session).toBeGreaterThan(0);
        expect(block.timeStamp).toBeDefined();
        // DHT Implementation is required for Collators
        // block = await service.addLastHeartBeat(block);
    });


    it('Will get the session number for a given block number', async ()=>{
        // const blockNumber=2468229;
        const blockNumber = 4494770;
        let block = {
            id: blockNumber,
            timeStamp: undefined,
        };
        block = await service.addBlockTiming(block);

        const nowd = new Date(parseInt(block.timeStamp));
        expect(nowd.getFullYear()).toBeGreaterThan(2016);

        // I can get the Relaychain block at the time. Works only for recent blocks.
        // const relayBlock = (await apiAt.query.parachainSystem.lastRelayChainBlockNumber()).toHuman();
    });

});
