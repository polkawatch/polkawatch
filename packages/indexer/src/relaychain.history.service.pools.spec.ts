// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { RelayChainAPIService, RelaychainHistoryService } from './relaychain.history.service';
import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { IdentityAPIService, IdentityService } from './identity.service';

// The API Connection may take time when connecting from far away
jest.setTimeout(40000);

describe('SubstrateService Pools', () => {
    let service: RelaychainHistoryService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    isGlobal: true,
                    validationSchema: Joi.object({
                        INDEXER_RELAYCHAIN_RPC_URL: Joi.string().default('wss://kusama.valletech.eu'),
                        INDEXER_IDENTITY_RPC_URL: Joi.string().default('wss://kusama-people-rpc.polkadot.io'),
                        INDEXER_CACHE_VALIDATORS: Joi.number().default(300),
                        INDEXER_CACHE_OPEN_ERAS: Joi.number().default(5),
                    }),
                }),
            ],
            providers: [RelayChainAPIService, RelaychainHistoryService, IdentityAPIService, IdentityService],
        }).compile();

        service = module.get<RelaychainHistoryService>(RelaychainHistoryService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('Will Check Returning Pool Root Account for a Nominator', async () => {
        // Positive Test
        let p = await service.getPoolAccount('F3opxRbN5ZavB4LTn2FZim9tPCHyvapGAAZzdDp5pzfg3sy');
        expect(p).toEqual('E8a4iJyDLd2ZysHt4bWfg5VG3RwNfHqSZtkyt5SPNJpmYoq');

        // Negative Test, when account is not pooled nominator

        p = await service.getPoolAccount('EJYeKKwU6Ua8H8TWqq85eRgAfcb1ZLneapYuR6FhRB5YgVL');
        expect(p).toBeUndefined();
    });


});