// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0


import { CacheModule, Module, DynamicModule } from '@nestjs/common';
import { ArchiveService } from './archive.service';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigModule } from '@nestjs/config';
import { RelayChainIndexerService } from './relaychain.indexer.service';
import { RelaychainHistoryService, RelayChainAPIService } from './relaychain.history.service';
import { ElasticService, ElasticApiClientService } from './elastic.service';
import { ParachainIndexerService } from './parachain.indexer.service';
import { ParachainHistoryService, ParahainAPIService } from './parachain.history.service';

import * as Joi from 'joi';
import { IdentityAPIService, IdentityService } from './identity.service';
import { ToporacleAPIService, ToporacleService } from './toporacle.service';

/**
 * The Core module includes a set of generic modules used across the Indexer
 * These modules are either very generic or NestJS provided
 */

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
            validationSchema: Joi.object({
                NODE_ENV: Joi.string()
                    .valid('development', 'production', 'test')
                    .default('development'),
                INDEXER_MODE: Joi.string().default('RELAY_NPOS'),
                INDEXER_PORT: Joi.number().default(7100),
                INDEXER_ARCHIVE_HOST: Joi.string().default('localhost'),
                INDEXER_ARCHIVE_PORT: Joi.number().default(3000),
                INDEXER_CACHE_VALIDATORS: Joi.number().default(300),
                INDEXER_CACHE_OPEN_ERAS: Joi.number().default(5),
                INDEXER_RELAYCHAIN_RPC_URL: Joi.string().default('wss://kusama.valletech.eu'),
                INDEXER_IDENTITY_RPC_URL: Joi.string().default('wss://kusama-people.valletech.eu'),
                INDEXER_RELAYCHAIN_HISTORY_DEPTH_MARGIN: Joi.number().default(0),
                INDEXER_RELAYCHAIN_HISTORY_ERAS_OVERRIDE: Joi.number().default(0),
                INDEXER_ELASTIC_PORT: Joi.number().default(9200),
                INDEXER_ELASTIC_HOST: Joi.string().default('localhost'),
                INDEXER_CRON_HOUR: Joi.number().default(19),
                INDEXER_PARACHAIN_RPC_URL: Joi.string().default('wss://statemine.valletech.eu'),
                INDEXER_TOPORACLE_URL: Joi.string().default('https://kusama-toporacle.valletech.eu'),
                INDEXER_TOPORACLE_PROXY: Joi.string().default(null),
            }),
        }),
        ScheduleModule.forRoot(),
        CacheModule.register(),
    ],
    exports: [ConfigModule, ScheduleModule, CacheModule],

})
class CoreModule {}


/**
 * Shared Modules across all Indexing configurations.
 */

@Module({
    imports: [CoreModule],
    providers: [IdentityAPIService, IdentityService, ArchiveService, RelayChainAPIService, RelaychainHistoryService, ToporacleAPIService, ToporacleService, ElasticService, ElasticApiClientService],
    exports: [IdentityService, ArchiveService, RelayChainAPIService, RelaychainHistoryService, ToporacleService, ElasticService, ElasticApiClientService ],
})
class IndexerCommonModule {}


/**
 * Configurable Indexer Module.
 * This module will be dynamically configured to serve the requested use case: RelayChain, ParaChain, etc.
 * //TODO: NestJS probably provides better ways to read configuration
 */
@Module({})
export class ConfigurableIndexerModule {
    static register():DynamicModule {
        let indexer_providers;
        if (process.env.INDEXER_MODE == 'PARACHAIN') indexer_providers = [ParachainIndexerService, ParachainHistoryService, ParahainAPIService ];
        else indexer_providers = [RelayChainIndexerService];
        return {
            module: ConfigurableIndexerModule,
            imports: [ IndexerCommonModule ],
            providers: indexer_providers,
        };
    }
}

/**
 * Indexer Module
 */
@Module({
    imports: [ConfigurableIndexerModule.register()],
})
export class IndexerModule {}