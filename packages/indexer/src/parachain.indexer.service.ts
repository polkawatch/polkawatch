// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Inject, Injectable, Logger } from '@nestjs/common';
import { ParachainHistoryService } from './parachain.history.service';
import { GeoliteService } from './geolite.service';
import { ElasticService } from './elastic.service';
import { ConfigService } from '@nestjs/config';
import { Cron, SchedulerRegistry, Timeout } from '@nestjs/schedule';

/**
 * Parachain indexer based on Block Authorship, for parachains that don't implement NPOS
 * and share identity responsibilities with RelayChain, such as Statemine
 */
@Injectable()
export class ParachainIndexerService {
    private readonly logger = new Logger(ParachainIndexerService.name);
    private lastIndexedBlock;
    private lastKnownBlock ;
    private indexing = true;

    constructor(
        private parachainHistory: ParachainHistoryService,
        @Inject('PARACHAIN_API') private api,
        private geoliteService: GeoliteService,
        private elasticService: ElasticService,
        private configService: ConfigService,
        private schedulerRegistry: SchedulerRegistry,
    ) {
        this.logger.log('Parachain Indexing Scheduler Starting...');
    }

    /**
     * Start indexing, but looking at the last indexed block if any.
     * After that, clear the lock so that the indexer can take place at regular intervals
     */
    @Timeout(1 * 1000)
    async startIndexing() {

        // retrieve the latest indexed block
        const lastBlockNumber = await this.elasticService.getLastRewardId();
        this.lastIndexedBlock = parseInt(lastBlockNumber);
        if(this.lastIndexedBlock) this.logger.log(`Last processed block was ${this.lastIndexedBlock}`);
        else this.logger.log('No last processed block found, initial Index');

        this.indexing = false;
    }

    /**
     * We subscribe after initial check,
     * The subscription is sometimes unreliable and may skip block numbers
     */
    @Timeout(5 * 1000)
    async listenToBlockSubscription() {
        this.logger.log('Listening to new parachain blocks...');

        await this.api.rpc.chain.subscribeNewHeads(async (header) => {
            this.logger.debug(`New block ${header.number} authored`);
            this.lastKnownBlock = header.number.toNumber();
        });
    }

    /**
     * Check if we need to continue indexing every 10 seconds
     * We catch up the index from last indexed to last known
     * @private
     */
    @Cron('*/10 * * * * *')
    private async continueIndexing() {
        // Ensure that we don't have multiple concurrent indexing operations
        if(this.indexing) return;
        this.indexing = true;

        if(this.lastIndexedBlock) {
            // Not the first time
            if(this.lastKnownBlock > this.lastIndexedBlock) await this.catchUpIndex(this.lastIndexedBlock + 1, this.lastKnownBlock);
        } else{
            // first time we index
            if(this.lastKnownBlock) await this.catchUpIndex(this.lastKnownBlock, this.lastKnownBlock);
        }

        this.indexing = false;
    }


    private async catchUpIndex(start, end) {
        if(end > start + 5) this.logger.log(`The Indexer needs to catch up from ${start} to ${end}`);
        for (let b = start; b <= end; b++) {
            this.logger.log(`Processing block ${b}`);
            await this.processBlock({ id:b });
        }
    }

    private async processBlock(block):Promise<any> {
        // Any processor that finds non-traceable information must report here:
        block.traced = true;

        try{
            block = await this.parachainHistory.processBlock(block);
            block = await this.geoliteService.processBlock(block);
            block = await this.elasticService.persistBlock(block);

            if(!block.traced) this.logger.debug(JSON.stringify(block));
        }catch (e) {
            this.logger.error(`Error while processing block ${block.id}: ${e.toString()}`);
        }

        this.lastIndexedBlock = block.id;

        return block;

    }
}