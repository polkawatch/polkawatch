// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0


import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { ToporacleApi, ToporacleAPIService, ToporacleService } from './toporacle.service';
import { GeoIpLocation } from '@polkawatch/toporacle-client';


// The API Connection may take time when connecting from far away
jest.setTimeout(20000);

describe('Toporacle Service', () => {
    let service: ToporacleService;
    let api: ToporacleApi;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    isGlobal: true,
                    validationSchema: Joi.object({
                        INDEXER_TOPORACLE_URL: Joi.string().default('https://kusama-toporacle.valletech.eu'),
                        INDEXER_CACHE_VALIDATORS: Joi.number().default(300),
                    }),
                }),
            ],
            providers: [ToporacleService, ToporacleAPIService],
        }).compile();

        service = module.get<ToporacleService>(ToporacleService);
        api = module.get<ToporacleApi>('TOPORACLE_API');
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
        expect(api).toBeDefined();
    });

    it('can get toporacle version', async () => {
        const version = await api.meta.getVersion();
        expect(version.data).toBeDefined();
    });

    it('Will resolve ip location of a well known validator', async () => {
        const validator = 'H3DL157HL7DkvV2kXocanmKaGXNyQphUDVW33Fnfk8KNhsv';

        const authority_info = await api.authority.getOwner({
            id: validator,
        });

        const location = authority_info.data.bestLocation;
        let geo_ip: GeoIpLocation;

        switch (location.discovery_type) {
        case 'Identified':
            geo_ip = location.geo_location;
            break;
        case 'Declared':
            // We take the first and Warn if multiple IPs resulted in multiple locations
            geo_ip = location.geo_locations[0];
            break;
        case 'Unknown':
                // We Warn
        }

        expect(geo_ip).toBeDefined();
    });


    it('Will check the reward location of a reward issue by a well known validator', async ()=>{
        const reward = await service.processReward({
            id: 'R1',
            validator: {
                id: 'H3DL157HL7DkvV2kXocanmKaGXNyQphUDVW33Fnfk8KNhsv',
            },
        });

        expect(reward.geo_country_display.country_code).toBe('DE');
        expect(reward.geo_asn_display.asn_name).toBe('Hetzner Online GmbH');
        expect(reward.geo_asn_display.asn_group_code).toBe('NOT_USED');

    });

});