// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0


import { Test, TestingModule } from '@nestjs/testing';
import { ArchiveService } from './archive.service';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';

describe('ArchiveService', () => {
    let archive: ArchiveService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    isGlobal: true,
                    validationSchema: Joi.object({
                        INDEXER_ARCHIVE_HOST: Joi.string().default('localhost'),
                        INDEXER_ARCHIVE_PORT: Joi.number().default(3000),
                        INDEXER_CACHE_VALIDATORS: Joi.number().default(300),
                        INDEXER_CACHE_OPEN_ERAS: Joi.number().default(5),
                    }),
                }),
            ],
            providers: [ArchiveService],
        }).compile();

        archive = module.get<ArchiveService>(ArchiveService);
    });

    it('should be defined', () => {
        expect(archive).toBeDefined();
    });

});
