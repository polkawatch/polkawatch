// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { Injectable, Inject, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiPromise, WsProvider } from '@polkadot/api';
import LRU from 'lru-cache';
import { RelaychainHistoryService } from './relaychain.history.service';


/**
 * Tracks decentralization of Parachain with consensus system Aura
 * The tracking is based on Block authorship, and it relays on
 * Relay Chain information for Heartbeat and Collator identification
 *
 * AuraParachins can mostly be indexed taking infromation from the Archive
 * node and the L2 index of the Relay chain.
 *
 * The data structure holding the information is as similar as possible to the
 * reward data structure.
 *
 */
@Injectable()
export class ParachainHistoryService {

    private readonly logger = new Logger(ParachainHistoryService.name);
    private apiCache;

    constructor(@Inject('PARACHAIN_API') private api, private relayChainHistory:RelaychainHistoryService, private configService: ConfigService) {
        this.logger.log('Parachain history service starting');
        this.apiCache = new LRU({ max:5 });

    }

    async processBlock(block): Promise<any> {
        block = await this.addBlockAuthorship(block);
        // DHT implementation required for Collators
        // block = await this.addLastHeartBeat(block);
        block = await this.addCollatorInfo(block);
        block = await this.addBlockTiming(block);
        block = this.addBlockMeta(block);

        return block;
    }

    /**
     * Returns the apiAt for a given block
     */
    async apiAt(block):Promise<any> {
        const blockHash = await this.api.rpc.chain.getBlockHash(block.id);
        return this.api.at(blockHash);
    }

    /**
     * Cached version of apiAt
     * @param block
     */
    async cachedApiAt(block):Promise<any> {
        const cacheKey = `api-${block.id}`;

        if (this.apiCache.has(cacheKey)) {return this.apiCache.get(cacheKey);} else {
            const apiAtPromise = this.apiAt(block);
            this.apiCache.set(cacheKey, apiAtPromise);
            return apiAtPromise;
        }
    }

    /**
     * Adds the collator that authored the block
     * @param block
     * @private
     */
    async addBlockAuthorship(block) {
        const blockHash = await this.api.rpc.chain.getBlockHash(block.id);
        const header = await this.api.derive.chain.getHeader(blockHash.toString());
        // the validator nomenclature here refers to a collator
        // format is compatible with NPOS reward indexing
        block.validatorId = header.author.toString();
        block.validator = { id:block.validatorId };
        return block;
    }

    /**
     * Adds block timestamp and session information
     * @param block
     */
    async addBlockTiming(block) {
        const apiAt = await this.cachedApiAt(block);

        // Adds session and timestamp as past queries
        block.session = (await apiAt.query.session.currentIndex()).toNumber();
        block.timeStamp = (await apiAt.query.timestamp.now()).toString();
        block.relayChainBlockNumber = (await apiAt.query.parachainSystem.lastRelayChainBlockNumber()).toNumber();
        return block;
    }

    /**
     * This is up to the blockchain how to implement. In this implementation, that will probably needs specialization,
     * identity is provided by the relay chain.
     *
     * @param block
     * @private
     */
    async addCollatorInfo(block) {
        block = await this.relayChainHistory.addValidatorInfo(block);
        return block;
    }

    /**
     * Resolves heartbeat form RelayChain at referenced BlockNumber
     * @param block
     * @private
     */
    async addLastHeartBeat(block) {
        // TODO: DHT implementation requried
        // return this.relayChainArchive.traceLastHeartbeat(block, block.relayChainBlockNumber);
        return block;
    }

    /**
     * Adds block metadata, and values meant to allow further processing in an NPOS compatible way.
     * @param block
     * @private
     */
    private addBlockMeta(block) {
        // With Aura consensus we count just blocks
        block.newReward = 1;
        block.rewardType = 'block';
        block.validatorType = 'authority';
        block.commission = 0;
        // reward type

        return block;
    }


}


/**
 * Associated provider of the RPC api object.
 * Will return the connected api object after configuration.
 */
export const ParahainAPIService = {
    provide: 'PARACHAIN_API',
    useFactory: async (configService: ConfigService) => {
        const endpoint = configService.get('INDEXER_PARACHAIN_RPC_URL');
        const logger = new Logger('PARACHAIN_API');
        const wsProvider = new WsProvider(endpoint);
        const api = await ApiPromise.create({ provider: wsProvider });
        await api.isReady;
        logger.log(`Parachain API Ready with endpoint: ${endpoint}`);
        logger.log(`Parachain is ${await api.rpc.system.chain()}`);
        logger.log(`Parachain ID is ${api.consts.system.ss58Prefix}`);
        return api;
    },
    inject: [ConfigService],
};