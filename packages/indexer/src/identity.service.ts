// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

import { ConfigService } from '@nestjs/config';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ApiPromise, WsProvider } from '@polkadot/api';
import { hexToU8a, isHex } from '@polkadot/util';
import LRU from 'lru-cache';


@Injectable()
export class IdentityService {
    private readonly tracing = false;
    private readonly logger = new Logger(IdentityService.name);
    private readonly identityCache;

    constructor(@Inject('IDENTITYCHAIN_API') private api, private configService: ConfigService) {
        // We want a very small cache, the purpose is not to hit constantly with all rewards of a validator...
        // but we want to check constantly for identity in case there are updates.
        const validators = configService.get('INDEXER_CACHE_VALIDATORS') / 100;
        this.identityCache = new LRU({ max:validators });
    }


    async getIdentity(accountId):Promise<any> {
        const identity = await this.api.query.identity.identityOf(accountId);
        if (identity.isSome) {
            let i = identity.unwrap().toHuman();
            i = Array.isArray(i) ? i[0] : i;
            return i.info;
        }
    }

    async cachedIdentity(accountId):Promise<any> {
        const cacheKey = `ide-${accountId}`;

        if (this.identityCache.has(cacheKey)) {return this.identityCache.get(cacheKey);} else {
            const identity = this.getIdentity(accountId);
            this.identityCache.set(cacheKey, identity);
            return identity;
        }
    }

    /**
     * Returns up to date substrate information about the account and its parent.
     * Accounts are typically a Validator or a Pool account but could be others too.
     * @param reward
     */
    async getAccountInfo(accountId):Promise<any> {
        const ret = {
            id: accountId,
            info: undefined,
            parentInfo: undefined,
            parentId: undefined,
            childId: undefined,
            display: undefined,
        };

        ret.info = await this.cachedIdentity(accountId);

        let accountSuper = await this.api.query.identity.superOf(accountId);
        if(accountSuper.isSome) {
            accountSuper = accountSuper.unwrap().toHuman();
            const accountParent = accountSuper[0];
            ret.parentId = accountParent;
            if(accountSuper.length > 1) ret.childId = accountSuper[1];
            ret.parentInfo = await this.cachedIdentity(accountParent);
        }

        // Add the Account information in its preferred display format
        ret.display = this.getAccountInfoDisplay(ret);

        if (this.tracing) this.logger.debug(`Account info for ${accountId} is ${JSON.stringify(ret)}`);
        return ret;
    }

    /**
     * The account information is presented to the user differently depending on the
     * parent/child available data
     * Accounts are typically a Validator or a Pool account but could be others too.
     * @param ai the available account information
     * */
    getAccountInfoDisplay(ai) {
        return {
            name: this.getAccountDisplayName(ai),
            identity: (ai.info || ai.parentInfo) ? true : false,
            parentId: ai.parentId ? ai.parentId : ai.id,
            groupName: this.getAccountGroupName(ai),
            groupWeb: this.getAccountGroupAttribute(ai, 'web'),
            groupEmail: this.getAccountGroupAttribute(ai, 'email'),
            groupLegal: this.getAccountGroupAttribute(ai, 'legal'),
            groupRiot: this.getAccountGroupAttribute(ai, 'riot'),
        };

    }

    /**
     * Workout the account display name depending on the different present information objects
     * Accounts are typically a Validator or a Pool account but could be others too.
     * @param ai the available account information
     */
    getAccountDisplayName(ai) {
        // the account itself has a
        if (ai.info) {
            if (ai.info.display) {return decodeInfoField(ai.info.display);}
        }
        if(ai.parentInfo && ai.childId) return `${this.getAccountGroupName(ai)} / ${this.getAccountChildId(ai)}`;
        return ai.id;
    }

    getAccountChildId(ai) {
        let ret = ai.id;
        if (ai.childId) {
            const childId = decodeInfoField(ai.childId);
            if(childId) ret = childId;
        }
        return ret;
    }

    /**
     * Workout the group name of the account
     * @param ai the available account information
     */
    getAccountGroupName(ai) {
        // if we don't have parent, we are it, use or same display name
        if(!ai.parentId) return this.getAccountDisplayName(ai);
        if(ai.parentInfo) return decodeInfoField(ai.parentInfo.display);
        else return ai.parentId;
    }

    /**
     * Return an account group data field,
     * @param ai the available account information
     * @param attr
     */
    getAccountGroupAttribute(ai, attr) {
        if(ai.parentInfo) {
            if(ai.parentInfo[attr]) {return decodeInfoField(ai.parentInfo[attr]);}
        }
    }

    /**
     * We close the api connection on module destroy
     */
    async onModuleDestroy() {
        this.logger.log('Closing API Connection...');
        await this.api.disconnect();
    }

}

/**
 * Will read an info field, detecting and decoding it in the case it is HEX
 */
export function decodeInfoField(field) {
    if(isHex(field.Raw)) return new Buffer(hexToU8a(field.Raw)).toString();
    else return field.Raw;
}

/**
 * Associated provider of the RPC api object.
 * Will return the connected api object after configuration.
 * Identities have now been moved to an specialized parachain.
 */

export const IdentityAPIService = {
    provide: 'IDENTITYCHAIN_API',
    useFactory: async (configService: ConfigService) => {
        const endpoint = configService.get('INDEXER_IDENTITY_RPC_URL');
        const logger = new Logger('IDENTITYCHAIN_API');
        const wsProvider = new WsProvider(endpoint);
        const api = await ApiPromise.create({ provider: wsProvider });
        await api.isReady;
        logger.log(`Identity API Ready with endpoint: ${endpoint}`);
        logger.log(`Identity Chain is ${await api.rpc.system.chain()}`);
        logger.log(`Identity Chain ID is ${api.consts.system.ss58Prefix}`);
        return api;
    },
    inject: [ConfigService],
};