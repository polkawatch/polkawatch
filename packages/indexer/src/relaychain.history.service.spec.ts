// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0


import { Test, TestingModule } from '@nestjs/testing';
import { RelaychainHistoryService, RelayChainAPIService } from './relaychain.history.service';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { ApiPromise } from '@polkadot/api';
import { IdentityAPIService, IdentityService } from './identity.service';

// The API Connection may take time when connecting from far away
jest.setTimeout(20000);

describe('RelayChain History Service', () => {
    let service: RelaychainHistoryService;
    let api: ApiPromise;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    isGlobal: true,
                    validationSchema: Joi.object({
                        INDEXER_RELAYCHAIN_RPC_URL: Joi.string().default('wss://kusama.valletech.eu'),
                        INDEXER_IDENTITY_RPC_URL: Joi.string().default('wss://kusama-people-rpc.polkadot.io'),
                        INDEXER_CACHE_VALIDATORS: Joi.number().default(300),
                        INDEXER_CACHE_OPEN_ERAS: Joi.number().default(5),
                    }),
                }),
            ],
            providers: [RelayChainAPIService, RelaychainHistoryService, IdentityAPIService, IdentityService],
        }).compile();

        service = module.get<RelaychainHistoryService>(RelaychainHistoryService);
        api = module.get<ApiPromise>('RELAYCHAIN_API');
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
        expect(api).toBeDefined();
    });

    it('will test a valid starting block', async () => {
        const startBlock = await service.historyDepthStartBlock();
        expect(startBlock).toBeGreaterThan(0);
    });

    /**
     * Will Check testing for pools wont break polkadot when not yet released
     */

    it('Will test Pools in polkadot', async () => {
        // Negative Test, when account is not pooled nominator
        const p = await service.getPoolAccount('14xEfVDASe2FYodUoNoXaksxyvnjxudjjQXkbXWgG5fVaxi8');
        expect(p).toBeUndefined();
    });

});
