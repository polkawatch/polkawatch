// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0


import { ApiPromise } from '@polkadot/api';
import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { decodeInfoField, IdentityAPIService, IdentityService } from './identity.service';

jest.setTimeout(20000);

describe('Identity Service', () => {
    let service: IdentityService;
    let api: ApiPromise;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    isGlobal: true,
                    validationSchema: Joi.object({
                        INDEXER_IDENTITY_RPC_URL: Joi.string().default('wss://kusama-people-rpc.polkadot.io'),
                        INDEXER_CACHE_VALIDATORS: Joi.number().default(300),

                    }),
                }),
            ],
            providers: [IdentityAPIService, IdentityService],
        }).compile();

        service = module.get<IdentityService>(IdentityService);
        api = module.get<ApiPromise>('IDENTITYCHAIN_API');

    });

    it('should be defined', () => {
        expect(service).toBeDefined();
        expect(api).toBeDefined();
    });

    // Test API primitives
    it('Will test api retrieving identity information from a well known validator', async ()=>{
        const accountId = 'Et9M3rrA7H2kHQEGRXHxufcp9HTEmFirMWtKHvjoJ85r1C9';
        const info = await service.getIdentity(accountId);
        expect(info).toBeDefined();
    });

    // Test Well Known Validator
    it('Will fetch validator: Anonymous validator with named parent, indexed', async ()=> {
        const info = await service.getAccountInfo('Hqk9zDKr84PF84ScDQJ29FHyGnKyQsXZPmLuAWFBgKZUybi');
        expect(info.info).toBeUndefined();
        expect(info.parentId).toBeDefined();
        expect(info.parentInfo).toBeDefined();
        expect(info.childId).toBeDefined();
    });

    // Test Well Known Validator
    it('Will fetch validator: Named validator with no parent', async ()=> {
        const info = await service.getAccountInfo('CczSz9z41uHpftVviWz91TgjLe3SmbvXfbAc958cjy7F6Qs');
        expect(info.info).toBeDefined();
        expect(info.parentId).toBeUndefined();
        expect(info.parentInfo).toBeUndefined();
        expect(info.childId).toBeUndefined();
    });

    // Test Well Known Validator
    it('Will fetch validator: anonymous validator with no parent', async ()=> {
        const info = await service.getAccountInfo('Caz16n5CBBBpnfETL6SpVWtanwygyJ5w8kdHmTuc8E9BNuf');
        expect(info.info).toBeUndefined();
        expect(info.parentId).toBeUndefined();
        expect(info.parentInfo).toBeUndefined();
        expect(info.childId).toBeUndefined();
    });


    /**
     * Pool info appears and is presented in several configurations
     */
    it('Will fetch pool info of a well known operators', async ()=>{

        // Anonymous pool with named parent, indexed.
        let info = await service.getAccountInfo('GypsNbingCkj1hovE5j1Nn7DLu53LMgHLDeddbVnCC3k1EN');
        expect(info.info).toBeUndefined();
        expect(info.parentId).toBeDefined();
        expect(info.parentInfo).toBeDefined();
        expect(info.childId).toBeDefined();

        // Named pool with no parent
        info = await service.getAccountInfo('Cgp9bcq1dGP1Z9B6F2ccTSTHNez9jq2iUX993ZbDVByPSU2');
        expect(info.info).toBeDefined();
        expect(info.parentId).toBeUndefined();
        expect(info.parentInfo).toBeUndefined();
        expect(info.childId).toBeUndefined();

        // anonymous pool with no parent
        info = await service.getAccountInfo('CowUfairmVenaJxGNw4F1bxfd9xmEC7d4R1PpPa2GYQzQT7');
        expect(info.info).toBeUndefined();
        expect(info.parentId).toBeUndefined();
        expect(info.parentInfo).toBeUndefined();
        expect(info.childId).toBeUndefined();
    });


    it('WIll check pool metadata Representation', async ()=>{
        // Pool metadata will not be stored in elastic but we just test its format.
        const f = decodeInfoField({ Raw:'0x54657374205374616b696e6720506f6f6c' });
        expect(f).toEqual('Test Staking Pool');
    });


});