// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0


import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { gql, GraphQLClient } from 'graphql-request';
import LRU from 'lru-cache';

/**
 * This Service access Polkawatch Archive GraphQL end point
 * through its query endpoints.
 *
 * Its main responsibility is to query all reward events in batches.
 *
 * Will also handle some edge cases in which heartbeats could not be
 * traced during the 1-pass archive process. The main reason is that
 * when you start mid-chain, there have been no heartbeats prior to the
 * very first rewards events. In those case Heartbeats are looked forward
 * and tagged as such.
 *
 * TODO: this implementation includes a misconception regarding the relationship between PeerId and ValidatorId.
 *
 */

@Injectable()
export class ArchiveService {
    private readonly logger = new Logger(ArchiveService.name);
    private readonly tracing = false;

    // We will not cache the main reward processing query
    private readonly client: GraphQLClient;

    private readonly peerCache;
    private readonly firstHeartbeatCache;
    private readonly lastHeartbeatCache;

    constructor(private configService: ConfigService) {
        const host = configService.get('INDEXER_ARCHIVE_HOST');
        const port = configService.get('INDEXER_ARCHIVE_PORT');
        this.client = new GraphQLClient(`http://${host}:${port}/graphql`, {
            headers: {},
        });

        // We want thin control of the Caches that we are using
        // for sizing we assume 300 active validators
        const validators = configService.get('INDEXER_CACHE_VALIDATORS');
        this.logger.log(`Cache sizing based on ${validators} validators`);
        this.peerCache = new LRU({ max:validators });
        this.firstHeartbeatCache = new LRU({ max:validators });
        this.lastHeartbeatCache = new LRU({ max:validators });
    }

    async query(query, params): Promise<any> {
        return this.client.request(query, params);
    }

    async queryRewards(params): Promise<any> {
        return this.query(REWARDS_QUERY, params);
    }

}

// The actual GraphQL queries associates with the methods above follow:

const REWARDS_QUERY = gql`
  query ($batchSize: Int!, $cursor: Cursor, $startBlockNumber: BigFloat) {
    rewards(
      first: $batchSize, 
      after: $cursor
      filter: {
        blockNumber: { greaterThan: $startBlockNumber }
      }
    ) {
      edges {
        cursor
        node {
          id
          timeStamp
          era
          blockNumber
          newReward
          nominator
          validator {
            id
            lastPeerId
          }
          payout {
            id
          }
          previousHeartbeat {
            id
            externalAddresses
          }
        }
      }
      pageInfo {
        hasNextPage
      }
    }
  }
`;
