// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0


import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
const { Client } = require('@elastic/elasticsearch');

@Injectable()
export class ElasticService {
    private readonly logger = new Logger(ElasticService.name);
    private readonly tracing = false;

    constructor(private readonly configService:ConfigService, @Inject('ELASTIC_API_CLIENT') private client) {
        // empty
    }

    /**
     * We perform an UPSERT (update or insert) of the Reward event. Note that the indexer handles live data and it is
     * scheduled to run everyday for history depth blocks.
     * @param reward
     */
    async persistReward(reward):Promise<any> {
        if(this.tracing) this.logger.debug(`Indexing reward ${reward.id}`);

        await this.client.update({
            index: 'pw_reward',
            id: reward.id,
            body: {
                doc: {
                    date: new Date(parseInt(reward.timeStamp)),
                    era: reward.era || reward.session,
                    traced: reward.traced,
                    reward: reward.newReward,
                    reward_commission: reward.commission,
                    reward_type: reward.rewardType,

                    nominator: reward.nominator,
                    nomination_value: reward.nominationExposure,

                    validator: reward.validator.id,
                    validator_type: reward.validatorType,
                    validator_identity: reward.validator.info.display.identity,

                    traced_heartbeat_id: reward.previousHeartbeat ? reward.previousHeartbeat.id : 'NOT_TRACED',
                    traced_heartbeat_type: reward.previousHeartbeatTrace,
                    traced_payout_id: reward.payout ? reward.payout.id : 'NOT_TRACED',

                    validator_parent: reward.validator.info.display.parentId,
                    validator_name: reward.validator.info.display.name,
                    validator_parent_name: reward.validator.info.display.groupName,
                    validator_parent_web: reward.validator.info.display.groupWeb,
                    validator_parent_email: reward.validator.info.display.groupEmail,
                    validator_parent_legal: reward.validator.info.display.groupLegal,
                    validator_parent_riot: reward.validator.info.display.groupRiot,

                    validator_country_group_code: reward.geo_country_display?.group_code,
                    validator_country_group_name: reward.geo_country_display?.group_name,
                    validator_country_code: reward.geo_country_display?.country_code,
                    validator_country_name: reward.geo_country_display?.country_name,
                    validator_asn_code: reward.geo_asn_display?.asn_code,
                    validator_asn_name: reward.geo_asn_display?.asn_name,
                    validator_asn_group_name: reward.geo_asn_display?.asn_group_name,
                    validator_asn_group_code: reward.geo_asn_display?.asn_group_code,

                    nominator_type: reward.nominator_type,
                    pool: reward.pool?.info.id,
                    pool_identity: reward.pool?.info.display.identity,
                    pool_name:  reward.pool?.info.display.name,
                    pool_parent: reward.pool?.info.display.parentId,
                    pool_parent_name: reward.pool?.info.display.groupName,
                    pool_parent_web: reward.pool?.info.display.groupWeb,
                    pool_parent_email: reward.pool?.info.display.groupEmail,
                    pool_parent_legal: reward.pool?.info.display.groupLegal,
                    pool_parent_riot: reward.pool?.info.display.groupRiot,

                },
                doc_as_upsert: true,
            },
        });
        return reward;
    }

    /**
     * Returns the latest ID to allow for a catch up indexing
     */

    async getLastRewardId() {
        const result = await this.client.search({
            'size': 1,
            'sort': [
                {
                    'date': {
                        'order': 'desc',
                    },
                },
            ],
            'aggs': {
                'last_document': {
                    'terms': {
                        'field': 'ID',
                        'size': 1,
                        'order': {
                            '_term': 'desc',
                        },
                    },
                },
            },
        },
        );
        if(result.hits.hits.length) return result.hits.hits[0]._id;
    }

    /**
     * Alternative entrypoint
     * @param block
     */
    async persistBlock(block):Promise<any> {
        return this.persistReward(block);
    }


    async onModuleDestroy() {
        this.logger.log('Closing API client down...');
        await this.client.close();
    }
}

/**
 * This provider offers a ready setup Elastic Client. Indexes will be created if they don't exist.
 */
export const ElasticApiClientService = {
    provide: 'ELASTIC_API_CLIENT',
    useFactory: async (configService: ConfigService) => {
        const logger = new Logger('ELASTIC_API_CLIENT');
        logger.log('Connecting to Elastic Search');
        const elasticHost = configService.get('INDEXER_ELASTIC_HOST');
        const elasticPort = configService.get('INDEXER_ELASTIC_PORT');
        const client = new Client({
            node: `http://${elasticHost}:${elasticPort}`,
        });

        const exists = await client.indices.exists({ index: 'pw_reward' });
        if(!exists) {
            logger.log('Creating reward index');
            await client.indices.create({
                index: 'pw_reward',
                body: {
                    mappings: {
                        properties: REWARD_PROPERTIES,
                    },
                },
            });
        }

        return client;
    },
    inject: [ConfigService],
};

/**
 * These are the reward event properties as specified by Elasticsearch
 */
const REWARD_PROPERTIES = {

    date: { type: 'date' },
    era: { type: 'long' },
    traced: { type: 'boolean' },
    nominator:  { type: 'keyword' },

    reward: { type: 'long' },
    reward_type: { type: 'keyword' },
    reward_commission: { type: 'float' },
    nomination_value: { type: 'long' },

    validator: { type: 'keyword' },
    validator_name: { type: 'keyword' },
    validator_type: { type: 'keyword' },
    validator_identity: { type: 'boolean' },

    // orphan validators will be their own parent (1 validator group)
    validator_parent: { type: 'keyword' },
    validator_parent_name: { type: 'keyword' },
    validator_parent_web: { type: 'keyword' },
    validator_parent_email: { type: 'keyword' },
    validator_parent_legal: { type: 'keyword' },
    validator_parent_riot: { type: 'keyword' },

    validator_country_group_code: { type: 'keyword' },
    validator_country_group_name: { type: 'keyword' },
    validator_country_code: { type: 'keyword' },
    validator_country_name: { type: 'keyword' },
    validator_asn_code: { type: 'keyword' },
    validator_asn_name: { type: 'keyword' },
    validator_asn_group_name: { type: 'keyword' },
    validator_asn_group_code: { type: 'keyword' },

    // Pool Support
    // orphan pools will be their own parent (1 validator group)
    nominator_type: { type: 'keyword' },
    // this will be the pool ROOT, not to be confused with Nominator above
    pool: { type: 'keyword' },
    pool_identity: { type: 'boolean' },
    pool_parent: { type: 'keyword' },
    pool_parent_name: { type: 'keyword' },
    pool_parent_web: { type: 'keyword' },
    pool_parent_email: { type: 'keyword' },
    pool_parent_legal: { type: 'keyword' },
    pool_parent_riot: { type: 'keyword' },

    // traceability, on-chain events related to this reward
    traced_heartbeat_id: { type: 'keyword' },
    traced_heartbeat_type: { type: 'keyword' },
    traced_payout_id: { type: 'keyword' },
};
