// Copyright 2021-2022 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0


import { Injectable, Inject, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { ApiPromise, WsProvider } from '@polkadot/api';
import LRU from 'lru-cache';
import { AccountId } from '@polkadot/types/interfaces';

import { IdentityService } from './identity.service';

/**
 * The Substrate History service uses RPC calls over History Depth to
 * extract some available information otherwise very difficult to deduce
 * from chain data.
 *
 * Essentially we are treating HistoryDepth queries as an "external" datasource
 * to merge data from.
 *
 * It might be possible to migrate some of this traces to pass-1 later on.
 *
 */

@Injectable()
export class RelaychainHistoryService {

    private readonly tracing = false;
    private readonly logger = new Logger(RelaychainHistoryService.name);

    // We will cache some expensive Substrate queries
    private exposureCache;
    private prefsCache;
    private validatorInfoCache;
    private poolInfoCache;
    private poolCache;

    constructor(@Inject('RELAYCHAIN_API') private api, private identityService: IdentityService, private configService: ConfigService) {
        // When caching we are sizing with the following assumptions
        // We consider 5 eras being claimed by 600 validators

        const validators = configService.get('INDEXER_CACHE_VALIDATORS');
        const openEras = configService.get('INDEXER_CACHE_OPEN_ERAS');
        this.logger.log(`Cache sizing based on ${validators} validators with ${openEras} open eras`);

        this.exposureCache = new LRU({ max:openEras * validators });
        this.prefsCache = new LRU({ max:openEras * validators });
        this.validatorInfoCache = new LRU({ max:validators * 1.3 });
        this.poolInfoCache = new LRU({ max:256 });
        this.poolCache = new LRU({ max:10000 });
    }

    /**
     * pass-2 indexing will start at history-depth.
     * external databases are known to contain "live data", for example geo data is constantly
     * amended, and must be updated daily as per license agreements.
     *
     * Substrate staking data is also "live" due to the fact that rewards can be claimed after a
     * certain time period, since data may be presented per era, the claiming process represent
     * "live" data too.
     *
     * history depth is considered a generous time period for live data to settle, and 2-pass
     * indexing should re-index from history depth every day.
     *
     * in production, we observed that when attempting to indexed from sharp history depth, some
     * involved blocks were already out of history depth (no exposure found, i.e.). history depth
     * margin is a tunable parameter to shorten history depth in eras.
     *
     */
    async historyDepthStartBlock():Promise<number> {
        let historyDepth;

        // ISSUE 64, API Change
        if(this.api.query.staking.historyDepth) historyDepth = (await this.api.query.staking.historyDepth()).toNumber();
        else historyDepth = await this.api.consts.staking.historyDepth.toNumber();

        // Sometimes historic information is already off session by the time we start indexing
        // We can setup some margin eras for not to start indexing right at history depth.

        const historyMargin = this.configService.get('INDEXER_RELAYCHAIN_HISTORY_DEPTH_MARGIN') ? this.configService.get('INDEXER_RELAYCHAIN_HISTORY_DEPTH_MARGIN') : 0;
        const epochDuration = await this.api.consts.babe.epochDuration.toNumber();
        const sessionsPerEra = await this.api.consts.staking.sessionsPerEra.toNumber();
        const blocksPerEra = sessionsPerEra * epochDuration;
        let historyBlocks = blocksPerEra * (historyDepth - historyMargin);

        // When reindexing we may just want to setup the number of history eras to reindex.
        // For example, if you want to re-index the last 7 eras everyday, then just setup as an override.

        const historyErasOverride = this.configService.get('INDEXER_RELAYCHAIN_HISTORY_ERAS_OVERRIDE');
        if(historyErasOverride) historyBlocks = blocksPerEra * historyErasOverride;

        const currentBlockNumber = await this.api.query.system.number();
        const startBlock = currentBlockNumber - historyBlocks;
        this.logger.log(`History Depth starts at block: ${startBlock}`);
        return startBlock;
    }

    async processReward(reward): Promise<any> {
        reward = await this.addEraExposure(reward);
        reward = await this.addValidatorInfo(reward);
        reward = await this.addValidationPoolInfo(reward);
        return reward;
    }

    /**
     *
     * This method will add additional information to the reward event that could not be traced
     * at chain archive time.
     *
     * This also allows us to validate the indexed data versus history-depth queries.
     *
     */

    async addEraExposure(reward):Promise<any> {
        const validatorId = reward.validator.id;
        const era = reward.era;
        const validatorPrefs = await this.getCachedValidatorPrefs(era, validatorId);

        // todo: Disabled, not working, since pagination, and should also fix the next issue
        // const exposureByStaker = await this.getCachedExposureByStaker(era, validatorId);
        // const exposure = exposureByStaker[reward.nominator];
        // This message may result also if the payout - reward trace is an error
        // becuase we dont find in history-depth a matching record of exposure
        // if(!exposure) this.logger.debug(`No EXPOSURE traced in ${reward.id}`);
        // reward.nominationExposure = exposureByStaker[reward.nominator];

        reward.commission = validatorPrefs.commission;

        // Here we try to caracterize the reward and validator for later analysis or grouping
        reward.validatorType = validatorPrefs.commission == 1 ? 'custodial' : 'public';
        reward.rewardType = reward.validator.id == reward.nominator ? 'validator commission' : 'staking reward';

        return reward;
    }

    /**
     * Cached version of Exposure by Staker
     * @param era
     * @param validatorId
     */
    async getCachedExposureByStaker(era, validatorId):Promise<any> {
        const cacheKey = `exposure-${validatorId}-${era}`;
        
        if (this.exposureCache.has(cacheKey)) {return this.exposureCache.get(cacheKey);} else {
            const valuePromise = this.getExposureByStaker(era, validatorId);
            this.exposureCache.set(cacheKey, valuePromise);
            return valuePromise;
        }
    }

    /**
     * Returns the exposure of nominators for a given era and validator id.
     * @param era
     * @param validatorId
     */
    async getExposureByStaker(era, validatorId):Promise<any> {
        if (this.tracing) this.logger.debug(`Requesting eraStakers for ${validatorId} era ${era}`);
        return this.api.query.staking.erasStakersPaged(era, validatorId, 2)
            .then(result => {
                const r = {};
                r[validatorId] = result.own.toBigInt().toString();
                result.others.forEach(exposure => r[exposure.who] = exposure.value.toBigInt().toString());
                return r;
            });
    }


    /**
     * Cached version of Validator Preferences
     * @param era
     * @param validatorId
     */
    async getCachedValidatorPrefs(era, validatorId):Promise<any> {
        const cacheKey = `prefs-${validatorId}-${era}`;

        if (this.prefsCache.has(cacheKey)) {return this.prefsCache.get(cacheKey);} else {
            const valuePromise = this.getValidatorPrefs(era, validatorId);
            this.prefsCache.set(cacheKey, valuePromise);
            return valuePromise;
        }
    }

    /**
    * Returns validator preferences for a given era
    * @param era
    * @param validatorId
    */
    async getValidatorPrefs(era, validatorId):Promise<any> {
        if (this.tracing) this.logger.debug(`Requesting validatorPrefs for ${validatorId} era ${era}`);
        return this.api.query.staking.erasValidatorPrefs(era, validatorId)
            .then(result => ({
                // Note this Number is a percentage and its representation is unrelated to tokenSymbols.
                commission: result.commission.toNumber() / 1000000000,
                blocked: result.blocked.toHuman(),
            }));
    }

    /**
     * Returns the validator information for a reward.
     * @param reward
     */
    async addValidatorInfo(reward): Promise<any> {
        reward.validator.info = await this.getCachedValidatorInfo(reward.validator.id);
        return reward;
    }

    /**
     * Cached version of validator info.
     * @param reward
     */
    async getCachedValidatorInfo(validatorId):Promise<any> {
        const cacheKey = `info-${validatorId}`;

        if (this.validatorInfoCache.has(cacheKey)) {return this.validatorInfoCache.get(cacheKey);} else {
            const valuePromise = await this.identityService.getAccountInfo(validatorId);
            this.validatorInfoCache.set(cacheKey, valuePromise);
            return valuePromise;
        }
    }

    /**
     * Adds Validation Pool information
     * @param reward
     */
    async addValidationPoolInfo(reward): Promise<any> {
        const info = await this.getCachedValidationPoolInfo(reward.nominator);
        if(info) {
            reward.pool = { info };
            reward.nominator_type = 'pooled';
        } else {reward.nominator_type = 'individual';}
        return reward;
    }

    /**
     * Cached version of validator pool info.
     * Note that here we have a bigger cache to remember which nominators are pools and
     * @param reward
     */
    async getCachedValidationPoolInfo(accountId):Promise<any> {
        let cacheKey = `poolId-${accountId}`;
        let poolId = undefined;

        if(this.poolCache.has(cacheKey)) {poolId = this.poolCache.get(cacheKey);} else {
            poolId = await this.getPoolAccount(accountId);
            this.poolCache.set(cacheKey, poolId);
        }

        if(poolId) {
            cacheKey = `poolInfo-${poolId}`;
            if (this.poolInfoCache.has(cacheKey)) {return this.poolInfoCache.get(cacheKey);} else {
                const valuePromise = this.identityService.getAccountInfo(poolId);
                this.poolInfoCache.set(cacheKey, valuePromise);
                return valuePromise;
            }
        }
    }


    /**
     * Returns the Validation Pool Root Account ID, if the Nominator belongs to a pool.
     */
    async getPoolAccount(account) {
        const poolId = (await this.api.query.nominationPools.reversePoolIdLookup(account as AccountId)).toHuman();
        if(poolId) {
            const poolState = (await this.api.query.nominationPools.bondedPools(parseInt(poolId))).toHuman();
            return poolState.roles.root;
        }
    }

    /**
     * We close the api connection on module destroy
     */
    async onModuleDestroy() {
        this.logger.log('Closing API Connection...');
        await this.api.disconnect();
    }
}

/**
 * Associated provider of the RPC api object.
 * Will return the connected api object after configuration.
 */
export const RelayChainAPIService = {
    provide: 'RELAYCHAIN_API',
    useFactory: async (configService: ConfigService) => {
        const endpoint = configService.get('INDEXER_RELAYCHAIN_RPC_URL');
        const logger = new Logger('RELAYCHAIN_API');
        const wsProvider = new WsProvider(endpoint);
        const api = await ApiPromise.create({ provider: wsProvider });
        await api.isReady;
        logger.log(`Relay Chain API Ready with endpoint: ${endpoint}`);
        logger.log(`Relay Chain is ${await api.rpc.system.chain()}`);
        logger.log(`Relay Chain ID is ${api.consts.system.ss58Prefix}`);
        return api;
    },
    inject: [ConfigService],
};

export const INDEXER_NO_OVERRIDE = 'e30K';