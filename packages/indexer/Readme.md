# Polkawatch Indexer

## Introduction

Polkawatch Indexing process takes place in 2 stages. In a first stage Polkawatch Archive extracts the
events from the blockchain as they are stored.

In the second stage, Polkawatch Indexer, performs the following tasks:

- Traces events that could not be properly traced during the Archive phase, in particular Heartbeats that could not be
matched to their validator using the session api are matched by using the PeerId as an intermediate key.
- Crosses the archived events with external datasources, in particular Geographic IP information and Network Provider
  (ASN) information.
- Without this architecture, the Indexer treats Substrate's History Depth API as an external datasource, and uses it to 
validate event relationships traced during the archive process as well as adding additional details to events.

Some of the external datasources are "live data", subject of a process of data quality, and can be udpated anytime. In 
particular, Geographic API data must be updated everyday according to license terms. 

The Indexer module is meant to re-index a generous part of the indexed events everyday. In particular all the History 
Depth is reindexed once a day.

## Parachain Support

From version 2, the indexer supports also Parachains. Note that it is common practice to request some parachain information,
such as identity, to the relay chain. 

For that reason the inders models a relay chain and parachain, so that it is architecturally possible to direct queries to both.

### Support for parachains wihtout NPOS

Some parachains do not implement NPOS / staking, as such it is not possible to analyze rewards to build decentralization infrastructure.
However, it is possible to analyze block authoring by collators and effectively equate block authoring to rewards.

Collators are "awarded" blocks each time they terminate/author one. The concept is close enough for architecture to be able
to model both rewards and block authoring as events to base decentralization.

In version 2 metadata is included to differentiate Relay Chains from Parachains and Staking from Block Authoring as events to 
base decentralization on.

## Technology stack

The module is based on [NetsJS](https://nestjs.com/)

# License and Copyright
Polkawatch is Open Source, Apache License Version 2.0.

©2022 Valletech AB, Authors and Contributors.